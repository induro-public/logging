import { __decorate, __param, __assign } from 'tslib';
import { InjectionToken, NgModule, Inject, ɵɵdefineInjectable, ɵɵinject, Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

var LOGGER_CONFIG = new InjectionToken('induro.logging.config');
var DEFAULT_LOG_ZONE = new InjectionToken('induro.logging.defaultZone');
var LOG_APPENDER = new InjectionToken('induro.logging.appender');
var InduroLogModule = /** @class */ (function () {
    function InduroLogModule() {
    }
    InduroLogModule_1 = InduroLogModule;
    InduroLogModule.initializeLogger = function (config) {
        return {
            ngModule: InduroLogModule_1,
            providers: [
                { provide: LOGGER_CONFIG, useValue: config },
                { provide: DEFAULT_LOG_ZONE, useValue: config.rootZone },
            ],
        };
    };
    var InduroLogModule_1;
    InduroLogModule = InduroLogModule_1 = __decorate([
        NgModule()
    ], InduroLogModule);
    return InduroLogModule;
}());

/**
 * Core log service that holds the pipeline for log messages.
 * Must be a singleton.
 * @export
 * @class LogCoreService
 */
var LogCoreService = /** @class */ (function () {
    function LogCoreService(appenders) {
        this.messagesStream = new Subject();
        this.messages.subscribe(function (logMessage) { return appenders.forEach(function (app) { return app.write(logMessage); }); });
    }
    Object.defineProperty(LogCoreService.prototype, "messages", {
        /**
         * Observable stream of log messages.
         * Meant for appenders to subscribe on.
         * @readonly
         * @type {Observable<LogMessage>}
         * @memberof LogService
         */
        get: function () {
            return this.messagesStream.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Log a message.
     * @memberof LogCoreService
     */
    LogCoreService.prototype.log = function (message) {
        this.messagesStream.next(message);
    };
    LogCoreService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: Inject, args: [LOG_APPENDER,] }] }
    ]; };
    LogCoreService.ɵprov = ɵɵdefineInjectable({ factory: function LogCoreService_Factory() { return new LogCoreService(ɵɵinject(LOG_APPENDER)); }, token: LogCoreService, providedIn: InduroLogModule });
    LogCoreService = __decorate([
        Injectable({ providedIn: InduroLogModule }),
        __param(0, Inject(LOG_APPENDER))
    ], LogCoreService);
    return LogCoreService;
}());

/**
 * Allow the user to modify the log settings live in the browser.
 * @export
 * @class LogSettingsService
 */
var LogSettingsService = /** @class */ (function () {
    function LogSettingsService(config) {
        this.logConfig = config;
        this.configChanges = new BehaviorSubject(__assign({}, this.logConfig));
    }
    Object.defineProperty(LogSettingsService.prototype, "config", {
        /**
         * Observe changes to the logging config.
         * @readonly
         * @type {Observable<LoggingConfig>}
         * @memberof LogSettingsService
         */
        get: function () {
            return this.configChanges.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogSettingsService.prototype, "level", {
        /**
         * Modify the log level.
         * @memberof LogSettingsService
         */
        set: function (logLevel) {
            this.logConfig.logLevel = logLevel;
            this.updateConfig();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogSettingsService.prototype, "zones", {
        /**
         * Set the zones.
         * @memberof LogSettingsService
         */
        set: function (zones) {
            this.logConfig.zones = new Set(zones);
            this.updateConfig();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Add a zone.
     * @memberof LogSettingsService
     */
    LogSettingsService.prototype.addZone = function (zone) {
        if (this.logConfig.zones.has(zone)) {
            return;
        }
        this.logConfig.zones.add(zone);
        this.updateConfig();
    };
    /**
     * Remove a zone;
     * @memberof LogSettingsService
     */
    LogSettingsService.prototype.removeZone = function (zone) {
        if (!this.logConfig.zones.delete(zone)) {
            return;
        }
        this.updateConfig();
    };
    /**
     * Publish a change for the config.
     * @private
     * @memberof LogSettingsService
     */
    LogSettingsService.prototype.updateConfig = function () {
        this.configChanges.next(__assign({}, this.logConfig));
    };
    LogSettingsService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [LOGGER_CONFIG,] }] }
    ]; };
    LogSettingsService.ɵprov = ɵɵdefineInjectable({ factory: function LogSettingsService_Factory() { return new LogSettingsService(ɵɵinject(LOGGER_CONFIG)); }, token: LogSettingsService, providedIn: InduroLogModule });
    LogSettingsService = __decorate([
        Injectable({ providedIn: InduroLogModule }),
        __param(0, Inject(LOGGER_CONFIG))
    ], LogSettingsService);
    return LogSettingsService;
}());

/**
 * Designates the levels for log messages and configuration.
 * @export
 * @enum {number}
 */
var LogLevel;
(function (LogLevel) {
    /**
     * Lowest level possible.
     */
    LogLevel[LogLevel["All"] = 0] = "All";
    /**
     * Fine-grained events used to track the flow of events.
     */
    LogLevel[LogLevel["Trace"] = 1] = "Trace";
    /**
     * Fine-grained informal events that are mostly useful to debug an application.
     */
    LogLevel[LogLevel["Debug"] = 2] = "Debug";
    /**
     * Informal message that highlights the progress of the application at coarse-grained level.
     */
    LogLevel[LogLevel["Info"] = 3] = "Info";
    /**
     * Potentially harmful situations.
     */
    LogLevel[LogLevel["Warn"] = 4] = "Warn";
    /**
     * Error events that might still allow the application to continue running.
     */
    LogLevel[LogLevel["Error"] = 5] = "Error";
    /**
     * Very severe error event that will presumably lead the application to abort.
     */
    LogLevel[LogLevel["Fatal"] = 6] = "Fatal";
    /**
     * Highest level possible.
     */
    LogLevel[LogLevel["Off"] = 7] = "Off";
})(LogLevel || (LogLevel = {}));

var LogMessage = /** @class */ (function () {
    function LogMessage(level, timestamp, message, zone, data, exception) {
        this.level = level;
        this.timestamp = timestamp;
        this.message = message;
        this.zone = zone;
        this.data = data;
        this.exception = exception;
    }
    Object.defineProperty(LogMessage.prototype, "levelText", {
        /**
         * Get the human readable version of the log level.
         * @readonly
         * @memberof LogMessage
         */
        // eslint-disable-next-line getter-return, consistent-return
        get: function () {
            // eslint-disable-next-line default-case
            switch (this.level) {
                case LogLevel.All:
                    return 'ALL';
                case LogLevel.Trace:
                    return 'TRACE';
                case LogLevel.Debug:
                    return 'DEBUG';
                case LogLevel.Info:
                    return 'INFO';
                case LogLevel.Warn:
                    return 'WARN';
                case LogLevel.Error:
                    return 'ERROR';
                case LogLevel.Fatal:
                    return 'FATAL';
                case LogLevel.Off:
                default:
                    return 'OFF';
            }
        },
        enumerable: true,
        configurable: true
    });
    return LogMessage;
}());

/**
 * Service used to persist log messages.
 * @export
 * @class LogService
 */
var LogService = /** @class */ (function () {
    // need a provider for log zone for the --prod build
    function LogService(logCore, logSettings, zone) {
        var _this = this;
        this.logCore = logCore;
        this.zone = zone;
        logSettings.config.subscribe(function (config) { return _this.onSettingsChange(config); });
        // setup method to create a zoned logger
        this.for = function (z) { return new LogService_1(logCore, logSettings, z); };
    }
    LogService_1 = LogService;
    Object.defineProperty(LogService.prototype, "isTraceEnabled", {
        /**
         * Is trace logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Trace;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isDebugEnabled", {
        /**
         * Is debug logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Debug;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isInfoEnabled", {
        /**
         * Is info logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Info;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isWarnEnabled", {
        /**
         * Is warn logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Warn;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isErrorEnabled", {
        /**
         * Is error logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Error;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isFatalEnabled", {
        /**
         * Is fatal logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Fatal;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Log a trace level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.trace = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Trace, message, data, exception);
    };
    /**
     * Log a debug level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.debug = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Debug, message, data, exception);
    };
    /**
     * Log an info level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.info = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Info, message, data, exception);
    };
    /**
     * Log a warn level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.warn = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Warn, message, data, exception);
    };
    /**
     * Log an error level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.error = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Error, message, data, exception);
    };
    /**
     * Log a fatal level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.fatal = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Fatal, message, data, exception);
    };
    /**
     * Write a log message for the given level if that level is enabled.
     * @private
     * @param level
     * @param message
     * @param [data=null]
     * @param [exception]
     * @memberof LogService
     */
    LogService.prototype.log = function (level, message, data, exception) {
        if (data === void 0) { data = null; }
        // filter by log level
        if (this.config.logLevel > level) {
            return;
        }
        // filter by zone
        if (this.config.zones.size > 0 && (!this.zone || !this.config.zones.has(this.zone))) {
            return;
        }
        var logMessage = new LogMessage(level, new Date(), message, this.zone, data, exception);
        this.logCore.log(logMessage);
    };
    /**
     * Update the log settings.
     * @private
     * @param config
     * @memberof LogService
     */
    LogService.prototype.onSettingsChange = function (config) {
        this.config = config;
    };
    var LogService_1;
    LogService.ctorParameters = function () { return [
        { type: LogCoreService },
        { type: LogSettingsService },
        { type: String, decorators: [{ type: Inject, args: [DEFAULT_LOG_ZONE,] }] }
    ]; };
    LogService.ɵprov = ɵɵdefineInjectable({ factory: function LogService_Factory() { return new LogService(ɵɵinject(LogCoreService), ɵɵinject(LogSettingsService), ɵɵinject(DEFAULT_LOG_ZONE)); }, token: LogService, providedIn: "root" });
    LogService = LogService_1 = __decorate([
        Injectable({ providedIn: 'root' }),
        __param(2, Inject(DEFAULT_LOG_ZONE))
    ], LogService);
    return LogService;
}());

var LOG_LEVEL_STYLES = new Map();
LOG_LEVEL_STYLES.set(LogLevel.Trace, 'font-weight: bold; color: #B0BEC5;');
LOG_LEVEL_STYLES.set(LogLevel.Debug, 'font-weight: bold; color: #4CAF50;');
LOG_LEVEL_STYLES.set(LogLevel.Info, 'font-weight: bold; color: #0277BD;');
LOG_LEVEL_STYLES.set(LogLevel.Warn, 'font-weight: bold; color: #FFC107;');
LOG_LEVEL_STYLES.set(LogLevel.Error, 'font-weight: bold; color: #B71C1C;');
LOG_LEVEL_STYLES.set(LogLevel.Fatal, 'font-weight: bold; color: #B71C1C;');
var ConsoleAppender = /** @class */ (function () {
    function ConsoleAppender() {
    }
    /**
     * Write a log message to the console.
     * @param logMessage
     * @memberof ConsoleAppender
     */
    ConsoleAppender.prototype.write = function (logMessage) {
        var style = LOG_LEVEL_STYLES.get(logMessage.level);
        if (logMessage.exception) {
            console.groupCollapsed(logMessage.timestamp.toLocaleString() + " [" + logMessage.levelText + "] (" + logMessage.zone + ")");
            console.log("%c" + logMessage.message, style);
            if (logMessage.data != null) {
                console.log(logMessage.data);
            }
            if (logMessage.exception) {
                console.log(logMessage.exception);
            }
            console.groupEnd();
        }
        else {
            var message = "%c" + logMessage.timestamp.toLocaleString() + " [" + logMessage.levelText + "] (" + logMessage.zone + ") - %c" + logMessage.message;
            if (logMessage.data != null) {
                console.log(message, style, '', logMessage.data);
            }
            else {
                console.log(message, style, '');
            }
        }
    };
    ConsoleAppender.ɵprov = ɵɵdefineInjectable({ factory: function ConsoleAppender_Factory() { return new ConsoleAppender(); }, token: ConsoleAppender, providedIn: InduroLogModule });
    ConsoleAppender = __decorate([
        Injectable({ providedIn: InduroLogModule })
    ], ConsoleAppender);
    return ConsoleAppender;
}());

/**
 * Generated bundle index. Do not edit.
 */

export { ConsoleAppender, InduroLogModule, LOG_APPENDER, LogLevel, LogService, LOGGER_CONFIG as ɵa, DEFAULT_LOG_ZONE as ɵb, LogCoreService as ɵc, LogSettingsService as ɵd };
//# sourceMappingURL=induro-logging.js.map
