import { __assign, __decorate, __param } from "tslib";
import { BehaviorSubject } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { InduroLogModule, LOGGER_CONFIG } from './log.module';
import * as i0 from "@angular/core";
import * as i1 from "./log.module";
/**
 * Allow the user to modify the log settings live in the browser.
 * @export
 * @class LogSettingsService
 */
var LogSettingsService = /** @class */ (function () {
    function LogSettingsService(config) {
        this.logConfig = config;
        this.configChanges = new BehaviorSubject(__assign({}, this.logConfig));
    }
    Object.defineProperty(LogSettingsService.prototype, "config", {
        /**
         * Observe changes to the logging config.
         * @readonly
         * @type {Observable<LoggingConfig>}
         * @memberof LogSettingsService
         */
        get: function () {
            return this.configChanges.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogSettingsService.prototype, "level", {
        /**
         * Modify the log level.
         * @memberof LogSettingsService
         */
        set: function (logLevel) {
            this.logConfig.logLevel = logLevel;
            this.updateConfig();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogSettingsService.prototype, "zones", {
        /**
         * Set the zones.
         * @memberof LogSettingsService
         */
        set: function (zones) {
            this.logConfig.zones = new Set(zones);
            this.updateConfig();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Add a zone.
     * @memberof LogSettingsService
     */
    LogSettingsService.prototype.addZone = function (zone) {
        if (this.logConfig.zones.has(zone)) {
            return;
        }
        this.logConfig.zones.add(zone);
        this.updateConfig();
    };
    /**
     * Remove a zone;
     * @memberof LogSettingsService
     */
    LogSettingsService.prototype.removeZone = function (zone) {
        if (!this.logConfig.zones.delete(zone)) {
            return;
        }
        this.updateConfig();
    };
    /**
     * Publish a change for the config.
     * @private
     * @memberof LogSettingsService
     */
    LogSettingsService.prototype.updateConfig = function () {
        this.configChanges.next(__assign({}, this.logConfig));
    };
    LogSettingsService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [LOGGER_CONFIG,] }] }
    ]; };
    LogSettingsService.ɵprov = i0.ɵɵdefineInjectable({ factory: function LogSettingsService_Factory() { return new LogSettingsService(i0.ɵɵinject(i1.LOGGER_CONFIG)); }, token: LogSettingsService, providedIn: i1.InduroLogModule });
    LogSettingsService = __decorate([
        Injectable({ providedIn: InduroLogModule }),
        __param(0, Inject(LOGGER_CONFIG))
    ], LogSettingsService);
    return LogSettingsService;
}());
export { LogSettingsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLXNldHRpbmdzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaW5kdXJvL2xvZ2dpbmcvIiwic291cmNlcyI6WyJsaWIvbG9nLXNldHRpbmdzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQXVCLE1BQU0sTUFBTSxDQUFDO0FBRTVELE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5ELE9BQU8sRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDOzs7QUFJOUQ7Ozs7R0FJRztBQUVIO0lBSUMsNEJBQW1DLE1BQXFCO1FBQ3ZELElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxlQUFlLGNBQXFCLElBQUksQ0FBQyxTQUFTLEVBQUcsQ0FBQztJQUNoRixDQUFDO0lBUUQsc0JBQVcsc0NBQU07UUFOakI7Ozs7O1dBS0c7YUFDSDtZQUNDLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMxQyxDQUFDOzs7T0FBQTtJQU1ELHNCQUFXLHFDQUFLO1FBSmhCOzs7V0FHRzthQUNILFVBQWlCLFFBQWtCO1lBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUNuQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckIsQ0FBQzs7O09BQUE7SUFNRCxzQkFBVyxxQ0FBSztRQUpoQjs7O1dBR0c7YUFDSCxVQUFpQixLQUFlO1lBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLElBQUksR0FBRyxDQUFTLEtBQUssQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNyQixDQUFDOzs7T0FBQTtJQUVEOzs7T0FHRztJQUNJLG9DQUFPLEdBQWQsVUFBZSxJQUFZO1FBQzFCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLE9BQU87U0FDUDtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLHVDQUFVLEdBQWpCLFVBQWtCLElBQVk7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN2QyxPQUFPO1NBQ1A7UUFDRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7O09BSUc7SUFDSyx5Q0FBWSxHQUFwQjtRQUNDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxjQUFNLElBQUksQ0FBQyxTQUFTLEVBQUcsQ0FBQztJQUNoRCxDQUFDOztnREEvRFksTUFBTSxTQUFDLGFBQWE7OztJQUpyQixrQkFBa0I7UUFEOUIsVUFBVSxDQUFDLEVBQUUsVUFBVSxFQUFFLGVBQWUsRUFBRSxDQUFDO1FBSzlCLFdBQUEsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFBO09BSnRCLGtCQUFrQixDQW9FOUI7NkJBbEZEO0NBa0ZDLEFBcEVELElBb0VDO1NBcEVZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBJbmR1cm9Mb2dNb2R1bGUsIExPR0dFUl9DT05GSUcgfSBmcm9tICcuL2xvZy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBMb2dnaW5nQ29uZmlnIH0gZnJvbSAnLi9tb2RlbHMvbG9nLWNvbmZpZy5tb2RlbCc7XHJcbmltcG9ydCB7IExvZ0xldmVsIH0gZnJvbSAnLi9tb2RlbHMvbG9nLWxldmVsLm1vZGVsJztcclxuXHJcbi8qKlxyXG4gKiBBbGxvdyB0aGUgdXNlciB0byBtb2RpZnkgdGhlIGxvZyBzZXR0aW5ncyBsaXZlIGluIHRoZSBicm93c2VyLlxyXG4gKiBAZXhwb3J0XHJcbiAqIEBjbGFzcyBMb2dTZXR0aW5nc1NlcnZpY2VcclxuICovXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogSW5kdXJvTG9nTW9kdWxlIH0pXHJcbmV4cG9ydCBjbGFzcyBMb2dTZXR0aW5nc1NlcnZpY2Uge1xyXG5cdHByaXZhdGUgbG9nQ29uZmlnOiBMb2dnaW5nQ29uZmlnO1xyXG5cdHByaXZhdGUgY29uZmlnQ2hhbmdlczogU3ViamVjdDxMb2dnaW5nQ29uZmlnPjtcclxuXHJcblx0Y29uc3RydWN0b3IoQEluamVjdChMT0dHRVJfQ09ORklHKSBjb25maWc6IExvZ2dpbmdDb25maWcpIHtcclxuXHRcdHRoaXMubG9nQ29uZmlnID0gY29uZmlnO1xyXG5cdFx0dGhpcy5jb25maWdDaGFuZ2VzID0gbmV3IEJlaGF2aW9yU3ViamVjdDxMb2dnaW5nQ29uZmlnPih7IC4uLnRoaXMubG9nQ29uZmlnIH0pO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogT2JzZXJ2ZSBjaGFuZ2VzIHRvIHRoZSBsb2dnaW5nIGNvbmZpZy5cclxuXHQgKiBAcmVhZG9ubHlcclxuXHQgKiBAdHlwZSB7T2JzZXJ2YWJsZTxMb2dnaW5nQ29uZmlnPn1cclxuXHQgKiBAbWVtYmVyb2YgTG9nU2V0dGluZ3NTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIGdldCBjb25maWcoKTogT2JzZXJ2YWJsZTxMb2dnaW5nQ29uZmlnPiB7XHJcblx0XHRyZXR1cm4gdGhpcy5jb25maWdDaGFuZ2VzLmFzT2JzZXJ2YWJsZSgpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogTW9kaWZ5IHRoZSBsb2cgbGV2ZWwuXHJcblx0ICogQG1lbWJlcm9mIExvZ1NldHRpbmdzU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBzZXQgbGV2ZWwobG9nTGV2ZWw6IExvZ0xldmVsKSB7XHJcblx0XHR0aGlzLmxvZ0NvbmZpZy5sb2dMZXZlbCA9IGxvZ0xldmVsO1xyXG5cdFx0dGhpcy51cGRhdGVDb25maWcoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFNldCB0aGUgem9uZXMuXHJcblx0ICogQG1lbWJlcm9mIExvZ1NldHRpbmdzU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBzZXQgem9uZXMoem9uZXM6IHN0cmluZ1tdKSB7XHJcblx0XHR0aGlzLmxvZ0NvbmZpZy56b25lcyA9IG5ldyBTZXQ8c3RyaW5nPih6b25lcyk7XHJcblx0XHR0aGlzLnVwZGF0ZUNvbmZpZygpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogQWRkIGEgem9uZS5cclxuXHQgKiBAbWVtYmVyb2YgTG9nU2V0dGluZ3NTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIGFkZFpvbmUoem9uZTogc3RyaW5nKTogdm9pZCB7XHJcblx0XHRpZiAodGhpcy5sb2dDb25maWcuem9uZXMuaGFzKHpvbmUpKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdHRoaXMubG9nQ29uZmlnLnpvbmVzLmFkZCh6b25lKTtcclxuXHRcdHRoaXMudXBkYXRlQ29uZmlnKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBSZW1vdmUgYSB6b25lO1xyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXR0aW5nc1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgcmVtb3ZlWm9uZSh6b25lOiBzdHJpbmcpOiB2b2lkIHtcclxuXHRcdGlmICghdGhpcy5sb2dDb25maWcuem9uZXMuZGVsZXRlKHpvbmUpKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdHRoaXMudXBkYXRlQ29uZmlnKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBQdWJsaXNoIGEgY2hhbmdlIGZvciB0aGUgY29uZmlnLlxyXG5cdCAqIEBwcml2YXRlXHJcblx0ICogQG1lbWJlcm9mIExvZ1NldHRpbmdzU2VydmljZVxyXG5cdCAqL1xyXG5cdHByaXZhdGUgdXBkYXRlQ29uZmlnKCk6IHZvaWQge1xyXG5cdFx0dGhpcy5jb25maWdDaGFuZ2VzLm5leHQoeyAuLi50aGlzLmxvZ0NvbmZpZyB9KTtcclxuXHR9XHJcbn1cclxuIl19