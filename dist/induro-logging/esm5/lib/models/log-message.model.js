import { LogLevel } from './log-level.model';
var LogMessage = /** @class */ (function () {
    function LogMessage(level, timestamp, message, zone, data, exception) {
        this.level = level;
        this.timestamp = timestamp;
        this.message = message;
        this.zone = zone;
        this.data = data;
        this.exception = exception;
    }
    Object.defineProperty(LogMessage.prototype, "levelText", {
        /**
         * Get the human readable version of the log level.
         * @readonly
         * @memberof LogMessage
         */
        // eslint-disable-next-line getter-return, consistent-return
        get: function () {
            // eslint-disable-next-line default-case
            switch (this.level) {
                case LogLevel.All:
                    return 'ALL';
                case LogLevel.Trace:
                    return 'TRACE';
                case LogLevel.Debug:
                    return 'DEBUG';
                case LogLevel.Info:
                    return 'INFO';
                case LogLevel.Warn:
                    return 'WARN';
                case LogLevel.Error:
                    return 'ERROR';
                case LogLevel.Fatal:
                    return 'FATAL';
                case LogLevel.Off:
                default:
                    return 'OFF';
            }
        },
        enumerable: true,
        configurable: true
    });
    return LogMessage;
}());
export { LogMessage };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLW1lc3NhZ2UubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaW5kdXJvL2xvZ2dpbmcvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2xvZy1tZXNzYWdlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUU3QztJQVFDLG9CQUFZLEtBQWUsRUFBRSxTQUFlLEVBQUUsT0FBZSxFQUFFLElBQWEsRUFBRSxJQUFVLEVBQUUsU0FBaUI7UUFDMUcsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDNUIsQ0FBQztJQVFELHNCQUFXLGlDQUFTO1FBTnBCOzs7O1dBSUc7UUFDSCw0REFBNEQ7YUFDNUQ7WUFDQyx3Q0FBd0M7WUFDeEMsUUFBUSxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNuQixLQUFLLFFBQVEsQ0FBQyxHQUFHO29CQUNoQixPQUFPLEtBQUssQ0FBQztnQkFDZCxLQUFLLFFBQVEsQ0FBQyxLQUFLO29CQUNsQixPQUFPLE9BQU8sQ0FBQztnQkFDaEIsS0FBSyxRQUFRLENBQUMsS0FBSztvQkFDbEIsT0FBTyxPQUFPLENBQUM7Z0JBQ2hCLEtBQUssUUFBUSxDQUFDLElBQUk7b0JBQ2pCLE9BQU8sTUFBTSxDQUFDO2dCQUNmLEtBQUssUUFBUSxDQUFDLElBQUk7b0JBQ2pCLE9BQU8sTUFBTSxDQUFDO2dCQUNmLEtBQUssUUFBUSxDQUFDLEtBQUs7b0JBQ2xCLE9BQU8sT0FBTyxDQUFDO2dCQUNoQixLQUFLLFFBQVEsQ0FBQyxLQUFLO29CQUNsQixPQUFPLE9BQU8sQ0FBQztnQkFDaEIsS0FBSyxRQUFRLENBQUMsR0FBRyxDQUFDO2dCQUNsQjtvQkFDQyxPQUFPLEtBQUssQ0FBQzthQUNkO1FBQ0YsQ0FBQzs7O09BQUE7SUFDRixpQkFBQztBQUFELENBQUMsQUE3Q0QsSUE2Q0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBMb2dMZXZlbCB9IGZyb20gJy4vbG9nLWxldmVsLm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBMb2dNZXNzYWdlIHtcclxuXHRsZXZlbDogTG9nTGV2ZWw7XHJcblx0dGltZXN0YW1wOiBEYXRlO1xyXG5cdG1lc3NhZ2U6IHN0cmluZztcclxuXHR6b25lPzogc3RyaW5nO1xyXG5cdGRhdGE/OiBhbnk7XHJcblx0ZXhjZXB0aW9uPzogRXJyb3I7XHJcblxyXG5cdGNvbnN0cnVjdG9yKGxldmVsOiBMb2dMZXZlbCwgdGltZXN0YW1wOiBEYXRlLCBtZXNzYWdlOiBzdHJpbmcsIHpvbmU/OiBzdHJpbmcsIGRhdGE/OiBhbnksIGV4Y2VwdGlvbj86IEVycm9yKSB7XHJcblx0XHR0aGlzLmxldmVsID0gbGV2ZWw7XHJcblx0XHR0aGlzLnRpbWVzdGFtcCA9IHRpbWVzdGFtcDtcclxuXHRcdHRoaXMubWVzc2FnZSA9IG1lc3NhZ2U7XHJcblx0XHR0aGlzLnpvbmUgPSB6b25lO1xyXG5cdFx0dGhpcy5kYXRhID0gZGF0YTtcclxuXHRcdHRoaXMuZXhjZXB0aW9uID0gZXhjZXB0aW9uO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogR2V0IHRoZSBodW1hbiByZWFkYWJsZSB2ZXJzaW9uIG9mIHRoZSBsb2cgbGV2ZWwuXHJcblx0ICogQHJlYWRvbmx5XHJcblx0ICogQG1lbWJlcm9mIExvZ01lc3NhZ2VcclxuXHQgKi9cclxuXHQvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZ2V0dGVyLXJldHVybiwgY29uc2lzdGVudC1yZXR1cm5cclxuXHRwdWJsaWMgZ2V0IGxldmVsVGV4dCgpOiBzdHJpbmcge1xyXG5cdFx0Ly8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGRlZmF1bHQtY2FzZVxyXG5cdFx0c3dpdGNoICh0aGlzLmxldmVsKSB7XHJcblx0XHRcdGNhc2UgTG9nTGV2ZWwuQWxsOlxyXG5cdFx0XHRcdHJldHVybiAnQUxMJztcclxuXHRcdFx0Y2FzZSBMb2dMZXZlbC5UcmFjZTpcclxuXHRcdFx0XHRyZXR1cm4gJ1RSQUNFJztcclxuXHRcdFx0Y2FzZSBMb2dMZXZlbC5EZWJ1ZzpcclxuXHRcdFx0XHRyZXR1cm4gJ0RFQlVHJztcclxuXHRcdFx0Y2FzZSBMb2dMZXZlbC5JbmZvOlxyXG5cdFx0XHRcdHJldHVybiAnSU5GTyc7XHJcblx0XHRcdGNhc2UgTG9nTGV2ZWwuV2FybjpcclxuXHRcdFx0XHRyZXR1cm4gJ1dBUk4nO1xyXG5cdFx0XHRjYXNlIExvZ0xldmVsLkVycm9yOlxyXG5cdFx0XHRcdHJldHVybiAnRVJST1InO1xyXG5cdFx0XHRjYXNlIExvZ0xldmVsLkZhdGFsOlxyXG5cdFx0XHRcdHJldHVybiAnRkFUQUwnO1xyXG5cdFx0XHRjYXNlIExvZ0xldmVsLk9mZjpcclxuXHRcdFx0ZGVmYXVsdDpcclxuXHRcdFx0XHRyZXR1cm4gJ09GRic7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdfQ==