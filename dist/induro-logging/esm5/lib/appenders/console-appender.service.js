import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { InduroLogModule } from '../log.module';
import { LogLevel } from '../models/log-level.model';
import * as i0 from "@angular/core";
import * as i1 from "../log.module";
var LOG_LEVEL_STYLES = new Map();
LOG_LEVEL_STYLES.set(LogLevel.Trace, 'font-weight: bold; color: #B0BEC5;');
LOG_LEVEL_STYLES.set(LogLevel.Debug, 'font-weight: bold; color: #4CAF50;');
LOG_LEVEL_STYLES.set(LogLevel.Info, 'font-weight: bold; color: #0277BD;');
LOG_LEVEL_STYLES.set(LogLevel.Warn, 'font-weight: bold; color: #FFC107;');
LOG_LEVEL_STYLES.set(LogLevel.Error, 'font-weight: bold; color: #B71C1C;');
LOG_LEVEL_STYLES.set(LogLevel.Fatal, 'font-weight: bold; color: #B71C1C;');
var ConsoleAppender = /** @class */ (function () {
    function ConsoleAppender() {
    }
    /**
     * Write a log message to the console.
     * @param logMessage
     * @memberof ConsoleAppender
     */
    ConsoleAppender.prototype.write = function (logMessage) {
        var style = LOG_LEVEL_STYLES.get(logMessage.level);
        if (logMessage.exception) {
            console.groupCollapsed(logMessage.timestamp.toLocaleString() + " [" + logMessage.levelText + "] (" + logMessage.zone + ")");
            console.log("%c" + logMessage.message, style);
            if (logMessage.data != null) {
                console.log(logMessage.data);
            }
            if (logMessage.exception) {
                console.log(logMessage.exception);
            }
            console.groupEnd();
        }
        else {
            var message = "%c" + logMessage.timestamp.toLocaleString() + " [" + logMessage.levelText + "] (" + logMessage.zone + ") - %c" + logMessage.message;
            if (logMessage.data != null) {
                console.log(message, style, '', logMessage.data);
            }
            else {
                console.log(message, style, '');
            }
        }
    };
    ConsoleAppender.ɵprov = i0.ɵɵdefineInjectable({ factory: function ConsoleAppender_Factory() { return new ConsoleAppender(); }, token: ConsoleAppender, providedIn: i1.InduroLogModule });
    ConsoleAppender = __decorate([
        Injectable({ providedIn: InduroLogModule })
    ], ConsoleAppender);
    return ConsoleAppender;
}());
export { ConsoleAppender };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc29sZS1hcHBlbmRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGluZHVyby9sb2dnaW5nLyIsInNvdXJjZXMiOlsibGliL2FwcGVuZGVycy9jb25zb2xlLWFwcGVuZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNoRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7OztBQUlyRCxJQUFNLGdCQUFnQixHQUFHLElBQUksR0FBRyxFQUFvQixDQUFDO0FBQ3JELGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLG9DQUFvQyxDQUFDLENBQUM7QUFDM0UsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztBQUMzRSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDO0FBQzFFLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLG9DQUFvQyxDQUFDLENBQUM7QUFDMUUsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsb0NBQW9DLENBQUMsQ0FBQztBQUMzRSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDO0FBRzNFO0lBQ0M7SUFBZSxDQUFDO0lBRWhCOzs7O09BSUc7SUFDSSwrQkFBSyxHQUFaLFVBQWEsVUFBc0I7UUFDbEMsSUFBTSxLQUFLLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVyRCxJQUFJLFVBQVUsQ0FBQyxTQUFTLEVBQUU7WUFDekIsT0FBTyxDQUFDLGNBQWMsQ0FBSSxVQUFVLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxVQUFLLFVBQVUsQ0FBQyxTQUFTLFdBQU0sVUFBVSxDQUFDLElBQUksTUFBRyxDQUFDLENBQUM7WUFDbEgsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFLLFVBQVUsQ0FBQyxPQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDOUMsSUFBSSxVQUFVLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDN0I7WUFDRCxJQUFJLFVBQVUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2xDO1lBQ0QsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ25CO2FBQU07WUFDTixJQUFNLE9BQU8sR0FBRyxPQUFLLFVBQVUsQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLFVBQUssVUFBVSxDQUFDLFNBQVMsV0FBTSxVQUFVLENBQUMsSUFBSSxjQUN2RyxVQUFVLENBQUMsT0FDVixDQUFDO1lBQ0gsSUFBSSxVQUFVLENBQUMsSUFBSSxJQUFJLElBQUksRUFBRTtnQkFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDakQ7aUJBQU07Z0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ2hDO1NBQ0Q7SUFDRixDQUFDOztJQS9CVyxlQUFlO1FBRDNCLFVBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsQ0FBQztPQUMvQixlQUFlLENBZ0MzQjswQkFoREQ7Q0FnREMsQUFoQ0QsSUFnQ0M7U0FoQ1ksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IEluZHVyb0xvZ01vZHVsZSB9IGZyb20gJy4uL2xvZy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBMb2dMZXZlbCB9IGZyb20gJy4uL21vZGVscy9sb2ctbGV2ZWwubW9kZWwnO1xyXG5pbXBvcnQgeyBMb2dNZXNzYWdlIH0gZnJvbSAnLi4vbW9kZWxzL2xvZy1tZXNzYWdlLm1vZGVsJztcclxuaW1wb3J0IHsgTG9nQXBwZW5kZXIgfSBmcm9tICcuL2xvZy1hcHBlbmRlci5zZXJ2aWNlJztcclxuXHJcbmNvbnN0IExPR19MRVZFTF9TVFlMRVMgPSBuZXcgTWFwPExvZ0xldmVsLCBzdHJpbmc+KCk7XHJcbkxPR19MRVZFTF9TVFlMRVMuc2V0KExvZ0xldmVsLlRyYWNlLCAnZm9udC13ZWlnaHQ6IGJvbGQ7IGNvbG9yOiAjQjBCRUM1OycpO1xyXG5MT0dfTEVWRUxfU1RZTEVTLnNldChMb2dMZXZlbC5EZWJ1ZywgJ2ZvbnQtd2VpZ2h0OiBib2xkOyBjb2xvcjogIzRDQUY1MDsnKTtcclxuTE9HX0xFVkVMX1NUWUxFUy5zZXQoTG9nTGV2ZWwuSW5mbywgJ2ZvbnQtd2VpZ2h0OiBib2xkOyBjb2xvcjogIzAyNzdCRDsnKTtcclxuTE9HX0xFVkVMX1NUWUxFUy5zZXQoTG9nTGV2ZWwuV2FybiwgJ2ZvbnQtd2VpZ2h0OiBib2xkOyBjb2xvcjogI0ZGQzEwNzsnKTtcclxuTE9HX0xFVkVMX1NUWUxFUy5zZXQoTG9nTGV2ZWwuRXJyb3IsICdmb250LXdlaWdodDogYm9sZDsgY29sb3I6ICNCNzFDMUM7Jyk7XHJcbkxPR19MRVZFTF9TVFlMRVMuc2V0KExvZ0xldmVsLkZhdGFsLCAnZm9udC13ZWlnaHQ6IGJvbGQ7IGNvbG9yOiAjQjcxQzFDOycpO1xyXG5cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiBJbmR1cm9Mb2dNb2R1bGUgfSlcclxuZXhwb3J0IGNsYXNzIENvbnNvbGVBcHBlbmRlciBpbXBsZW1lbnRzIExvZ0FwcGVuZGVyIHtcclxuXHRjb25zdHJ1Y3RvcigpIHt9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFdyaXRlIGEgbG9nIG1lc3NhZ2UgdG8gdGhlIGNvbnNvbGUuXHJcblx0ICogQHBhcmFtIGxvZ01lc3NhZ2VcclxuXHQgKiBAbWVtYmVyb2YgQ29uc29sZUFwcGVuZGVyXHJcblx0ICovXHJcblx0cHVibGljIHdyaXRlKGxvZ01lc3NhZ2U6IExvZ01lc3NhZ2UpOiB2b2lkIHtcclxuXHRcdGNvbnN0IHN0eWxlID0gTE9HX0xFVkVMX1NUWUxFUy5nZXQobG9nTWVzc2FnZS5sZXZlbCk7XHJcblxyXG5cdFx0aWYgKGxvZ01lc3NhZ2UuZXhjZXB0aW9uKSB7XHJcblx0XHRcdGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQoYCR7bG9nTWVzc2FnZS50aW1lc3RhbXAudG9Mb2NhbGVTdHJpbmcoKX0gWyR7bG9nTWVzc2FnZS5sZXZlbFRleHR9XSAoJHtsb2dNZXNzYWdlLnpvbmV9KWApO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhgJWMke2xvZ01lc3NhZ2UubWVzc2FnZX1gLCBzdHlsZSk7XHJcblx0XHRcdGlmIChsb2dNZXNzYWdlLmRhdGEgIT0gbnVsbCkge1xyXG5cdFx0XHRcdGNvbnNvbGUubG9nKGxvZ01lc3NhZ2UuZGF0YSk7XHJcblx0XHRcdH1cclxuXHRcdFx0aWYgKGxvZ01lc3NhZ2UuZXhjZXB0aW9uKSB7XHJcblx0XHRcdFx0Y29uc29sZS5sb2cobG9nTWVzc2FnZS5leGNlcHRpb24pO1xyXG5cdFx0XHR9XHJcblx0XHRcdGNvbnNvbGUuZ3JvdXBFbmQoKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdGNvbnN0IG1lc3NhZ2UgPSBgJWMke2xvZ01lc3NhZ2UudGltZXN0YW1wLnRvTG9jYWxlU3RyaW5nKCl9IFske2xvZ01lc3NhZ2UubGV2ZWxUZXh0fV0gKCR7bG9nTWVzc2FnZS56b25lfSkgLSAlYyR7XHJcblx0XHRcdFx0bG9nTWVzc2FnZS5tZXNzYWdlXHJcblx0XHRcdH1gO1xyXG5cdFx0XHRpZiAobG9nTWVzc2FnZS5kYXRhICE9IG51bGwpIHtcclxuXHRcdFx0XHRjb25zb2xlLmxvZyhtZXNzYWdlLCBzdHlsZSwgJycsIGxvZ01lc3NhZ2UuZGF0YSk7XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0Y29uc29sZS5sb2cobWVzc2FnZSwgc3R5bGUsICcnKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=