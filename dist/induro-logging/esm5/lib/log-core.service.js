import { __decorate, __param } from "tslib";
import { Subject } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { InduroLogModule, LOG_APPENDER } from './log.module';
import * as i0 from "@angular/core";
import * as i1 from "./log.module";
/**
 * Core log service that holds the pipeline for log messages.
 * Must be a singleton.
 * @export
 * @class LogCoreService
 */
var LogCoreService = /** @class */ (function () {
    function LogCoreService(appenders) {
        this.messagesStream = new Subject();
        this.messages.subscribe(function (logMessage) { return appenders.forEach(function (app) { return app.write(logMessage); }); });
    }
    Object.defineProperty(LogCoreService.prototype, "messages", {
        /**
         * Observable stream of log messages.
         * Meant for appenders to subscribe on.
         * @readonly
         * @type {Observable<LogMessage>}
         * @memberof LogService
         */
        get: function () {
            return this.messagesStream.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Log a message.
     * @memberof LogCoreService
     */
    LogCoreService.prototype.log = function (message) {
        this.messagesStream.next(message);
    };
    LogCoreService.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: Inject, args: [LOG_APPENDER,] }] }
    ]; };
    LogCoreService.ɵprov = i0.ɵɵdefineInjectable({ factory: function LogCoreService_Factory() { return new LogCoreService(i0.ɵɵinject(i1.LOG_APPENDER)); }, token: LogCoreService, providedIn: i1.InduroLogModule });
    LogCoreService = __decorate([
        Injectable({ providedIn: InduroLogModule }),
        __param(0, Inject(LOG_APPENDER))
    ], LogCoreService);
    return LogCoreService;
}());
export { LogCoreService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLWNvcmUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BpbmR1cm8vbG9nZ2luZy8iLCJzb3VyY2VzIjpbImxpYi9sb2ctY29yZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBR25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLE1BQU0sY0FBYyxDQUFDOzs7QUFHN0Q7Ozs7O0dBS0c7QUFFSDtJQUdDLHdCQUFrQyxTQUF3QjtRQUN6RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksT0FBTyxFQUFjLENBQUM7UUFDaEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQyxVQUFVLElBQUssT0FBQSxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsRUFBckIsQ0FBcUIsQ0FBQyxFQUFqRCxDQUFpRCxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQVNELHNCQUFXLG9DQUFRO1FBUG5COzs7Ozs7V0FNRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzNDLENBQUM7OztPQUFBO0lBRUQ7OztPQUdHO0lBQ0ksNEJBQUcsR0FBVixVQUFXLE9BQW1CO1FBQzdCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ25DLENBQUM7OzRDQXRCWSxNQUFNLFNBQUMsWUFBWTs7O0lBSHBCLGNBQWM7UUFEMUIsVUFBVSxDQUFDLEVBQUUsVUFBVSxFQUFFLGVBQWUsRUFBRSxDQUFDO1FBSTlCLFdBQUEsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFBO09BSHJCLGNBQWMsQ0EwQjFCO3lCQXpDRDtDQXlDQyxBQTFCRCxJQTBCQztTQTFCWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBMb2dBcHBlbmRlciB9IGZyb20gJy4vYXBwZW5kZXJzL2xvZy1hcHBlbmRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5kdXJvTG9nTW9kdWxlLCBMT0dfQVBQRU5ERVIgfSBmcm9tICcuL2xvZy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBMb2dNZXNzYWdlIH0gZnJvbSAnLi9tb2RlbHMvbG9nLW1lc3NhZ2UubW9kZWwnO1xyXG5cclxuLyoqXHJcbiAqIENvcmUgbG9nIHNlcnZpY2UgdGhhdCBob2xkcyB0aGUgcGlwZWxpbmUgZm9yIGxvZyBtZXNzYWdlcy5cclxuICogTXVzdCBiZSBhIHNpbmdsZXRvbi5cclxuICogQGV4cG9ydFxyXG4gKiBAY2xhc3MgTG9nQ29yZVNlcnZpY2VcclxuICovXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogSW5kdXJvTG9nTW9kdWxlIH0pXHJcbmV4cG9ydCBjbGFzcyBMb2dDb3JlU2VydmljZSB7XHJcblx0cHJpdmF0ZSBtZXNzYWdlc1N0cmVhbTogU3ViamVjdDxMb2dNZXNzYWdlPjtcclxuXHJcblx0Y29uc3RydWN0b3IoQEluamVjdChMT0dfQVBQRU5ERVIpIGFwcGVuZGVyczogTG9nQXBwZW5kZXJbXSkge1xyXG5cdFx0dGhpcy5tZXNzYWdlc1N0cmVhbSA9IG5ldyBTdWJqZWN0PExvZ01lc3NhZ2U+KCk7XHJcblx0XHR0aGlzLm1lc3NhZ2VzLnN1YnNjcmliZSgobG9nTWVzc2FnZSkgPT4gYXBwZW5kZXJzLmZvckVhY2goKGFwcCkgPT4gYXBwLndyaXRlKGxvZ01lc3NhZ2UpKSk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBPYnNlcnZhYmxlIHN0cmVhbSBvZiBsb2cgbWVzc2FnZXMuXHJcblx0ICogTWVhbnQgZm9yIGFwcGVuZGVycyB0byBzdWJzY3JpYmUgb24uXHJcblx0ICogQHJlYWRvbmx5XHJcblx0ICogQHR5cGUge09ic2VydmFibGU8TG9nTWVzc2FnZT59XHJcblx0ICogQG1lbWJlcm9mIExvZ1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgZ2V0IG1lc3NhZ2VzKCk6IE9ic2VydmFibGU8TG9nTWVzc2FnZT4ge1xyXG5cdFx0cmV0dXJuIHRoaXMubWVzc2FnZXNTdHJlYW0uYXNPYnNlcnZhYmxlKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBMb2cgYSBtZXNzYWdlLlxyXG5cdCAqIEBtZW1iZXJvZiBMb2dDb3JlU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBsb2cobWVzc2FnZTogTG9nTWVzc2FnZSk6IHZvaWQge1xyXG5cdFx0dGhpcy5tZXNzYWdlc1N0cmVhbS5uZXh0KG1lc3NhZ2UpO1xyXG5cdH1cclxufVxyXG4iXX0=