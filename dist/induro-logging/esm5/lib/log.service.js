import { __decorate, __param } from "tslib";
import { Inject, Injectable } from '@angular/core';
import { LogCoreService } from './log-core.service';
import { LogSettingsService } from './log-settings.service';
import { DEFAULT_LOG_ZONE } from './log.module';
import { LogLevel } from './models/log-level.model';
import { LogMessage } from './models/log-message.model';
import * as i0 from "@angular/core";
import * as i1 from "./log-core.service";
import * as i2 from "./log-settings.service";
import * as i3 from "./log.module";
/**
 * Service used to persist log messages.
 * @export
 * @class LogService
 */
var LogService = /** @class */ (function () {
    // need a provider for log zone for the --prod build
    function LogService(logCore, logSettings, zone) {
        var _this = this;
        this.logCore = logCore;
        this.zone = zone;
        logSettings.config.subscribe(function (config) { return _this.onSettingsChange(config); });
        // setup method to create a zoned logger
        this.for = function (z) { return new LogService_1(logCore, logSettings, z); };
    }
    LogService_1 = LogService;
    Object.defineProperty(LogService.prototype, "isTraceEnabled", {
        /**
         * Is trace logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Trace;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isDebugEnabled", {
        /**
         * Is debug logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Debug;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isInfoEnabled", {
        /**
         * Is info logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Info;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isWarnEnabled", {
        /**
         * Is warn logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Warn;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isErrorEnabled", {
        /**
         * Is error logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Error;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LogService.prototype, "isFatalEnabled", {
        /**
         * Is fatal logging enabled?
         * @readonly
         * @memberof LogService
         */
        get: function () {
            return this.config.logLevel <= LogLevel.Fatal;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Log a trace level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.trace = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Trace, message, data, exception);
    };
    /**
     * Log a debug level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.debug = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Debug, message, data, exception);
    };
    /**
     * Log an info level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.info = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Info, message, data, exception);
    };
    /**
     * Log a warn level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.warn = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Warn, message, data, exception);
    };
    /**
     * Log an error level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.error = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Error, message, data, exception);
    };
    /**
     * Log a fatal level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    LogService.prototype.fatal = function (message, data, exception) {
        if (data === void 0) { data = null; }
        this.log(LogLevel.Fatal, message, data, exception);
    };
    /**
     * Write a log message for the given level if that level is enabled.
     * @private
     * @param level
     * @param message
     * @param [data=null]
     * @param [exception]
     * @memberof LogService
     */
    LogService.prototype.log = function (level, message, data, exception) {
        if (data === void 0) { data = null; }
        // filter by log level
        if (this.config.logLevel > level) {
            return;
        }
        // filter by zone
        if (this.config.zones.size > 0 && (!this.zone || !this.config.zones.has(this.zone))) {
            return;
        }
        var logMessage = new LogMessage(level, new Date(), message, this.zone, data, exception);
        this.logCore.log(logMessage);
    };
    /**
     * Update the log settings.
     * @private
     * @param config
     * @memberof LogService
     */
    LogService.prototype.onSettingsChange = function (config) {
        this.config = config;
    };
    var LogService_1;
    LogService.ctorParameters = function () { return [
        { type: LogCoreService },
        { type: LogSettingsService },
        { type: String, decorators: [{ type: Inject, args: [DEFAULT_LOG_ZONE,] }] }
    ]; };
    LogService.ɵprov = i0.ɵɵdefineInjectable({ factory: function LogService_Factory() { return new LogService(i0.ɵɵinject(i1.LogCoreService), i0.ɵɵinject(i2.LogSettingsService), i0.ɵɵinject(i3.DEFAULT_LOG_ZONE)); }, token: LogService, providedIn: "root" });
    LogService = LogService_1 = __decorate([
        Injectable({ providedIn: 'root' }),
        __param(2, Inject(DEFAULT_LOG_ZONE))
    ], LogService);
    return LogService;
}());
export { LogService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaW5kdXJvL2xvZ2dpbmcvIiwic291cmNlcyI6WyJsaWIvbG9nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFaEQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7Ozs7QUFFeEQ7Ozs7R0FJRztBQUVIO0lBS0Msb0RBQW9EO0lBQ3BELG9CQUNTLE9BQXVCLEVBQy9CLFdBQStCLEVBQ0wsSUFBYTtRQUh4QyxpQkFTQztRQVJRLFlBQU8sR0FBUCxPQUFPLENBQWdCO1FBSS9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLFdBQVcsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUMsTUFBTSxJQUFLLE9BQUEsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUE3QixDQUE2QixDQUFDLENBQUM7UUFDeEUsd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBQyxDQUFTLElBQUssT0FBQSxJQUFJLFlBQVUsQ0FBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUF2QyxDQUF1QyxDQUFDO0lBQ25FLENBQUM7bUJBZlcsVUFBVTtJQXNCdEIsc0JBQVcsc0NBQWM7UUFMekI7Ozs7V0FJRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBT0Qsc0JBQVcsc0NBQWM7UUFMekI7Ozs7V0FJRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBT0Qsc0JBQVcscUNBQWE7UUFMeEI7Ozs7V0FJRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQzlDLENBQUM7OztPQUFBO0lBT0Qsc0JBQVcscUNBQWE7UUFMeEI7Ozs7V0FJRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQzlDLENBQUM7OztPQUFBO0lBT0Qsc0JBQVcsc0NBQWM7UUFMekI7Ozs7V0FJRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBT0Qsc0JBQVcsc0NBQWM7UUFMekI7Ozs7V0FJRzthQUNIO1lBQ0MsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBRUQ7Ozs7OztPQU1HO0lBQ0ksMEJBQUssR0FBWixVQUFhLE9BQWUsRUFBRSxJQUFnQixFQUFFLFNBQWlCO1FBQW5DLHFCQUFBLEVBQUEsV0FBZ0I7UUFDN0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNJLDBCQUFLLEdBQVosVUFBYSxPQUFlLEVBQUUsSUFBZ0IsRUFBRSxTQUFpQjtRQUFuQyxxQkFBQSxFQUFBLFdBQWdCO1FBQzdDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSx5QkFBSSxHQUFYLFVBQVksT0FBZSxFQUFFLElBQWdCLEVBQUUsU0FBaUI7UUFBbkMscUJBQUEsRUFBQSxXQUFnQjtRQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0kseUJBQUksR0FBWCxVQUFZLE9BQWUsRUFBRSxJQUFnQixFQUFFLFNBQWlCO1FBQW5DLHFCQUFBLEVBQUEsV0FBZ0I7UUFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNJLDBCQUFLLEdBQVosVUFBYSxPQUFlLEVBQUUsSUFBZ0IsRUFBRSxTQUFpQjtRQUFuQyxxQkFBQSxFQUFBLFdBQWdCO1FBQzdDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSSwwQkFBSyxHQUFaLFVBQWEsT0FBZSxFQUFFLElBQWdCLEVBQUUsU0FBaUI7UUFBbkMscUJBQUEsRUFBQSxXQUFnQjtRQUM3QyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQ7Ozs7Ozs7O09BUUc7SUFDSyx3QkFBRyxHQUFYLFVBQVksS0FBZSxFQUFFLE9BQWUsRUFBRSxJQUFnQixFQUFFLFNBQWlCO1FBQW5DLHFCQUFBLEVBQUEsV0FBZ0I7UUFDN0Qsc0JBQXNCO1FBQ3RCLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsS0FBSyxFQUFFO1lBQ2pDLE9BQU87U0FDUDtRQUNELGlCQUFpQjtRQUNqQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUU7WUFDcEYsT0FBTztTQUNQO1FBRUQsSUFBTSxVQUFVLEdBQUcsSUFBSSxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzFGLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLHFDQUFnQixHQUF4QixVQUF5QixNQUFxQjtRQUM3QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN0QixDQUFDOzs7Z0JBaktpQixjQUFjO2dCQUNsQixrQkFBa0I7NkNBQzlCLE1BQU0sU0FBQyxnQkFBZ0I7OztJQVRiLFVBQVU7UUFEdEIsVUFBVSxDQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFDO1FBVWhDLFdBQUEsTUFBTSxDQUFDLGdCQUFnQixDQUFDLENBQUE7T0FUZCxVQUFVLENBeUt0QjtxQkF4TEQ7Q0F3TEMsQUF6S0QsSUF5S0M7U0F6S1ksVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTG9nQ29yZVNlcnZpY2UgfSBmcm9tICcuL2xvZy1jb3JlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXR0aW5nc1NlcnZpY2UgfSBmcm9tICcuL2xvZy1zZXR0aW5ncy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgREVGQVVMVF9MT0dfWk9ORSB9IGZyb20gJy4vbG9nLm1vZHVsZSc7XHJcbmltcG9ydCB7IExvZ2dpbmdDb25maWcgfSBmcm9tICcuL21vZGVscy9sb2ctY29uZmlnLm1vZGVsJztcclxuaW1wb3J0IHsgTG9nTGV2ZWwgfSBmcm9tICcuL21vZGVscy9sb2ctbGV2ZWwubW9kZWwnO1xyXG5pbXBvcnQgeyBMb2dNZXNzYWdlIH0gZnJvbSAnLi9tb2RlbHMvbG9nLW1lc3NhZ2UubW9kZWwnO1xyXG5cclxuLyoqXHJcbiAqIFNlcnZpY2UgdXNlZCB0byBwZXJzaXN0IGxvZyBtZXNzYWdlcy5cclxuICogQGV4cG9ydFxyXG4gKiBAY2xhc3MgTG9nU2VydmljZVxyXG4gKi9cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcclxuZXhwb3J0IGNsYXNzIExvZ1NlcnZpY2Uge1xyXG5cdHB1YmxpYyBmb3I6ICh6b25lOiBzdHJpbmcpID0+IExvZ1NlcnZpY2U7XHJcblx0cHJpdmF0ZSB6b25lOiBzdHJpbmcgfCB1bmRlZmluZWQ7XHJcblx0cHJpdmF0ZSBjb25maWchOiBMb2dnaW5nQ29uZmlnO1xyXG5cclxuXHQvLyBuZWVkIGEgcHJvdmlkZXIgZm9yIGxvZyB6b25lIGZvciB0aGUgLS1wcm9kIGJ1aWxkXHJcblx0Y29uc3RydWN0b3IoXHJcblx0XHRwcml2YXRlIGxvZ0NvcmU6IExvZ0NvcmVTZXJ2aWNlLFxyXG5cdFx0bG9nU2V0dGluZ3M6IExvZ1NldHRpbmdzU2VydmljZSxcclxuXHRcdEBJbmplY3QoREVGQVVMVF9MT0dfWk9ORSkgem9uZT86IHN0cmluZ1xyXG5cdCkge1xyXG5cdFx0dGhpcy56b25lID0gem9uZTtcclxuXHRcdGxvZ1NldHRpbmdzLmNvbmZpZy5zdWJzY3JpYmUoKGNvbmZpZykgPT4gdGhpcy5vblNldHRpbmdzQ2hhbmdlKGNvbmZpZykpO1xyXG5cdFx0Ly8gc2V0dXAgbWV0aG9kIHRvIGNyZWF0ZSBhIHpvbmVkIGxvZ2dlclxyXG5cdFx0dGhpcy5mb3IgPSAoejogc3RyaW5nKSA9PiBuZXcgTG9nU2VydmljZShsb2dDb3JlLCBsb2dTZXR0aW5ncywgeik7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBJcyB0cmFjZSBsb2dnaW5nIGVuYWJsZWQ/XHJcblx0ICogQHJlYWRvbmx5XHJcblx0ICogQG1lbWJlcm9mIExvZ1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgZ2V0IGlzVHJhY2VFbmFibGVkKCk6IGJvb2xlYW4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuY29uZmlnLmxvZ0xldmVsIDw9IExvZ0xldmVsLlRyYWNlO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogSXMgZGVidWcgbG9nZ2luZyBlbmFibGVkP1xyXG5cdCAqIEByZWFkb25seVxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIGdldCBpc0RlYnVnRW5hYmxlZCgpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiB0aGlzLmNvbmZpZy5sb2dMZXZlbCA8PSBMb2dMZXZlbC5EZWJ1ZztcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIElzIGluZm8gbG9nZ2luZyBlbmFibGVkP1xyXG5cdCAqIEByZWFkb25seVxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIGdldCBpc0luZm9FbmFibGVkKCk6IGJvb2xlYW4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuY29uZmlnLmxvZ0xldmVsIDw9IExvZ0xldmVsLkluZm87XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBJcyB3YXJuIGxvZ2dpbmcgZW5hYmxlZD9cclxuXHQgKiBAcmVhZG9ubHlcclxuXHQgKiBAbWVtYmVyb2YgTG9nU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBnZXQgaXNXYXJuRW5hYmxlZCgpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiB0aGlzLmNvbmZpZy5sb2dMZXZlbCA8PSBMb2dMZXZlbC5XYXJuO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogSXMgZXJyb3IgbG9nZ2luZyBlbmFibGVkP1xyXG5cdCAqIEByZWFkb25seVxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIGdldCBpc0Vycm9yRW5hYmxlZCgpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiB0aGlzLmNvbmZpZy5sb2dMZXZlbCA8PSBMb2dMZXZlbC5FcnJvcjtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIElzIGZhdGFsIGxvZ2dpbmcgZW5hYmxlZD9cclxuXHQgKiBAcmVhZG9ubHlcclxuXHQgKiBAbWVtYmVyb2YgTG9nU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBnZXQgaXNGYXRhbEVuYWJsZWQoKTogYm9vbGVhbiB7XHJcblx0XHRyZXR1cm4gdGhpcy5jb25maWcubG9nTGV2ZWwgPD0gTG9nTGV2ZWwuRmF0YWw7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBMb2cgYSB0cmFjZSBsZXZlbCBtZXNzYWdlIGlmIHRoYXQgbGV2ZWwgaXMgZW5hYmxlZC5cclxuXHQgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG5cdCAqIEBwYXJhbSBbZGF0YT1udWxsXSBEYXRhIHRoYXQgcHJvdmlkZXMgY29udGV4dCBmb3IgdGhlIG1lc3NhZ2UuXHJcblx0ICogQHBhcmFtIFtleGNlcHRpb25dIEVycm9yIHJlbGF0ZWQgdG8gdGhlIG1lc3NhZ2UuXHJcblx0ICogQG1lbWJlcm9mIExvZ1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgdHJhY2UobWVzc2FnZTogc3RyaW5nLCBkYXRhOiBhbnkgPSBudWxsLCBleGNlcHRpb24/OiBFcnJvcik6IHZvaWQge1xyXG5cdFx0dGhpcy5sb2coTG9nTGV2ZWwuVHJhY2UsIG1lc3NhZ2UsIGRhdGEsIGV4Y2VwdGlvbik7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBMb2cgYSBkZWJ1ZyBsZXZlbCBtZXNzYWdlIGlmIHRoYXQgbGV2ZWwgaXMgZW5hYmxlZC5cclxuXHQgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG5cdCAqIEBwYXJhbSBbZGF0YT1udWxsXSBEYXRhIHRoYXQgcHJvdmlkZXMgY29udGV4dCBmb3IgdGhlIG1lc3NhZ2UuXHJcblx0ICogQHBhcmFtIFtleGNlcHRpb25dIEVycm9yIHJlbGF0ZWQgdG8gdGhlIG1lc3NhZ2UuXHJcblx0ICogQG1lbWJlcm9mIExvZ1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgZGVidWcobWVzc2FnZTogc3RyaW5nLCBkYXRhOiBhbnkgPSBudWxsLCBleGNlcHRpb24/OiBFcnJvcik6IHZvaWQge1xyXG5cdFx0dGhpcy5sb2coTG9nTGV2ZWwuRGVidWcsIG1lc3NhZ2UsIGRhdGEsIGV4Y2VwdGlvbik7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBMb2cgYW4gaW5mbyBsZXZlbCBtZXNzYWdlIGlmIHRoYXQgbGV2ZWwgaXMgZW5hYmxlZC5cclxuXHQgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG5cdCAqIEBwYXJhbSBbZGF0YT1udWxsXSBEYXRhIHRoYXQgcHJvdmlkZXMgY29udGV4dCBmb3IgdGhlIG1lc3NhZ2UuXHJcblx0ICogQHBhcmFtIFtleGNlcHRpb25dIEVycm9yIHJlbGF0ZWQgdG8gdGhlIG1lc3NhZ2UuXHJcblx0ICogQG1lbWJlcm9mIExvZ1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgaW5mbyhtZXNzYWdlOiBzdHJpbmcsIGRhdGE6IGFueSA9IG51bGwsIGV4Y2VwdGlvbj86IEVycm9yKTogdm9pZCB7XHJcblx0XHR0aGlzLmxvZyhMb2dMZXZlbC5JbmZvLCBtZXNzYWdlLCBkYXRhLCBleGNlcHRpb24pO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogTG9nIGEgd2FybiBsZXZlbCBtZXNzYWdlIGlmIHRoYXQgbGV2ZWwgaXMgZW5hYmxlZC5cclxuXHQgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG5cdCAqIEBwYXJhbSBbZGF0YT1udWxsXSBEYXRhIHRoYXQgcHJvdmlkZXMgY29udGV4dCBmb3IgdGhlIG1lc3NhZ2UuXHJcblx0ICogQHBhcmFtIFtleGNlcHRpb25dIEVycm9yIHJlbGF0ZWQgdG8gdGhlIG1lc3NhZ2UuXHJcblx0ICogQG1lbWJlcm9mIExvZ1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgd2FybihtZXNzYWdlOiBzdHJpbmcsIGRhdGE6IGFueSA9IG51bGwsIGV4Y2VwdGlvbj86IEVycm9yKTogdm9pZCB7XHJcblx0XHR0aGlzLmxvZyhMb2dMZXZlbC5XYXJuLCBtZXNzYWdlLCBkYXRhLCBleGNlcHRpb24pO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogTG9nIGFuIGVycm9yIGxldmVsIG1lc3NhZ2UgaWYgdGhhdCBsZXZlbCBpcyBlbmFibGVkLlxyXG5cdCAqIEBwYXJhbSBtZXNzYWdlIE1lc3NhZ2UgdG8gbG9nXHJcblx0ICogQHBhcmFtIFtkYXRhPW51bGxdIERhdGEgdGhhdCBwcm92aWRlcyBjb250ZXh0IGZvciB0aGUgbWVzc2FnZS5cclxuXHQgKiBAcGFyYW0gW2V4Y2VwdGlvbl0gRXJyb3IgcmVsYXRlZCB0byB0aGUgbWVzc2FnZS5cclxuXHQgKiBAbWVtYmVyb2YgTG9nU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBlcnJvcihtZXNzYWdlOiBzdHJpbmcsIGRhdGE6IGFueSA9IG51bGwsIGV4Y2VwdGlvbj86IEVycm9yKTogdm9pZCB7XHJcblx0XHR0aGlzLmxvZyhMb2dMZXZlbC5FcnJvciwgbWVzc2FnZSwgZGF0YSwgZXhjZXB0aW9uKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIExvZyBhIGZhdGFsIGxldmVsIG1lc3NhZ2UgaWYgdGhhdCBsZXZlbCBpcyBlbmFibGVkLlxyXG5cdCAqIEBwYXJhbSBtZXNzYWdlIE1lc3NhZ2UgdG8gbG9nXHJcblx0ICogQHBhcmFtIFtkYXRhPW51bGxdIERhdGEgdGhhdCBwcm92aWRlcyBjb250ZXh0IGZvciB0aGUgbWVzc2FnZS5cclxuXHQgKiBAcGFyYW0gW2V4Y2VwdGlvbl0gRXJyb3IgcmVsYXRlZCB0byB0aGUgbWVzc2FnZS5cclxuXHQgKiBAbWVtYmVyb2YgTG9nU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyBmYXRhbChtZXNzYWdlOiBzdHJpbmcsIGRhdGE6IGFueSA9IG51bGwsIGV4Y2VwdGlvbj86IEVycm9yKTogdm9pZCB7XHJcblx0XHR0aGlzLmxvZyhMb2dMZXZlbC5GYXRhbCwgbWVzc2FnZSwgZGF0YSwgZXhjZXB0aW9uKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFdyaXRlIGEgbG9nIG1lc3NhZ2UgZm9yIHRoZSBnaXZlbiBsZXZlbCBpZiB0aGF0IGxldmVsIGlzIGVuYWJsZWQuXHJcblx0ICogQHByaXZhdGVcclxuXHQgKiBAcGFyYW0gbGV2ZWxcclxuXHQgKiBAcGFyYW0gbWVzc2FnZVxyXG5cdCAqIEBwYXJhbSBbZGF0YT1udWxsXVxyXG5cdCAqIEBwYXJhbSBbZXhjZXB0aW9uXVxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXJ2aWNlXHJcblx0ICovXHJcblx0cHJpdmF0ZSBsb2cobGV2ZWw6IExvZ0xldmVsLCBtZXNzYWdlOiBzdHJpbmcsIGRhdGE6IGFueSA9IG51bGwsIGV4Y2VwdGlvbj86IEVycm9yKTogdm9pZCB7XHJcblx0XHQvLyBmaWx0ZXIgYnkgbG9nIGxldmVsXHJcblx0XHRpZiAodGhpcy5jb25maWcubG9nTGV2ZWwgPiBsZXZlbCkge1xyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblx0XHQvLyBmaWx0ZXIgYnkgem9uZVxyXG5cdFx0aWYgKHRoaXMuY29uZmlnLnpvbmVzLnNpemUgPiAwICYmICghdGhpcy56b25lIHx8ICF0aGlzLmNvbmZpZy56b25lcy5oYXModGhpcy56b25lKSkpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cclxuXHRcdGNvbnN0IGxvZ01lc3NhZ2UgPSBuZXcgTG9nTWVzc2FnZShsZXZlbCwgbmV3IERhdGUoKSwgbWVzc2FnZSwgdGhpcy56b25lLCBkYXRhLCBleGNlcHRpb24pO1xyXG5cdFx0dGhpcy5sb2dDb3JlLmxvZyhsb2dNZXNzYWdlKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFVwZGF0ZSB0aGUgbG9nIHNldHRpbmdzLlxyXG5cdCAqIEBwcml2YXRlXHJcblx0ICogQHBhcmFtIGNvbmZpZ1xyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXJ2aWNlXHJcblx0ICovXHJcblx0cHJpdmF0ZSBvblNldHRpbmdzQ2hhbmdlKGNvbmZpZzogTG9nZ2luZ0NvbmZpZyk6IHZvaWQge1xyXG5cdFx0dGhpcy5jb25maWcgPSBjb25maWc7XHJcblx0fVxyXG59XHJcbiJdfQ==