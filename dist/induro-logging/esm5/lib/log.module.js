import { __decorate } from "tslib";
import { InjectionToken, NgModule } from '@angular/core';
export var LOGGER_CONFIG = new InjectionToken('induro.logging.config');
export var DEFAULT_LOG_ZONE = new InjectionToken('induro.logging.defaultZone');
export var LOG_APPENDER = new InjectionToken('induro.logging.appender');
var InduroLogModule = /** @class */ (function () {
    function InduroLogModule() {
    }
    InduroLogModule_1 = InduroLogModule;
    InduroLogModule.initializeLogger = function (config) {
        return {
            ngModule: InduroLogModule_1,
            providers: [
                { provide: LOGGER_CONFIG, useValue: config },
                { provide: DEFAULT_LOG_ZONE, useValue: config.rootZone },
            ],
        };
    };
    var InduroLogModule_1;
    InduroLogModule = InduroLogModule_1 = __decorate([
        NgModule()
    ], InduroLogModule);
    return InduroLogModule;
}());
export { InduroLogModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BpbmR1cm8vbG9nZ2luZy8iLCJzb3VyY2VzIjpbImxpYi9sb2cubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBWSxNQUFNLGVBQWUsQ0FBQztBQUtuRSxNQUFNLENBQUMsSUFBTSxhQUFhLEdBQUcsSUFBSSxjQUFjLENBQW9CLHVCQUF1QixDQUFDLENBQUM7QUFDNUYsTUFBTSxDQUFDLElBQU0sZ0JBQWdCLEdBQTJCLElBQUksY0FBYyxDQUFTLDRCQUE0QixDQUFDLENBQUM7QUFDakgsTUFBTSxDQUFDLElBQU0sWUFBWSxHQUFnQyxJQUFJLGNBQWMsQ0FBUyx5QkFBeUIsQ0FBQyxDQUFDO0FBRy9HO0lBQUE7SUFVQSxDQUFDO3dCQVZZLGVBQWU7SUFDcEIsZ0NBQWdCLEdBQXZCLFVBQXdCLE1BQXlCO1FBQ2hELE9BQU87WUFDTixRQUFRLEVBQUUsaUJBQWU7WUFDekIsU0FBUyxFQUFFO2dCQUNWLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFO2dCQUM1QyxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRTthQUN4RDtTQUNELENBQUM7SUFDSCxDQUFDOztJQVRXLGVBQWU7UUFEM0IsUUFBUSxFQUFFO09BQ0UsZUFBZSxDQVUzQjtJQUFELHNCQUFDO0NBQUEsQUFWRCxJQVVDO1NBVlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGlvblRva2VuLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IExvZ0FwcGVuZGVyIH0gZnJvbSAnLi9hcHBlbmRlcnMvbG9nLWFwcGVuZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dnZXJTZXR1cENvbmZpZyB9IGZyb20gJy4vbW9kZWxzL2xvZ2dlci1zZXR1cC1jb25maWcubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNvbnN0IExPR0dFUl9DT05GSUcgPSBuZXcgSW5qZWN0aW9uVG9rZW48TG9nZ2VyU2V0dXBDb25maWc+KCdpbmR1cm8ubG9nZ2luZy5jb25maWcnKTtcclxuZXhwb3J0IGNvbnN0IERFRkFVTFRfTE9HX1pPTkU6IEluamVjdGlvblRva2VuPHN0cmluZz4gPSBuZXcgSW5qZWN0aW9uVG9rZW48c3RyaW5nPignaW5kdXJvLmxvZ2dpbmcuZGVmYXVsdFpvbmUnKTtcclxuZXhwb3J0IGNvbnN0IExPR19BUFBFTkRFUjogSW5qZWN0aW9uVG9rZW48TG9nQXBwZW5kZXI+ID0gbmV3IEluamVjdGlvblRva2VuPHN0cmluZz4oJ2luZHVyby5sb2dnaW5nLmFwcGVuZGVyJyk7XHJcblxyXG5ATmdNb2R1bGUoKVxyXG5leHBvcnQgY2xhc3MgSW5kdXJvTG9nTW9kdWxlIHtcclxuXHRzdGF0aWMgaW5pdGlhbGl6ZUxvZ2dlcihjb25maWc6IExvZ2dlclNldHVwQ29uZmlnKTogeyBuZ01vZHVsZTogdHlwZW9mIEluZHVyb0xvZ01vZHVsZTsgcHJvdmlkZXJzOiBQcm92aWRlcltdIH0ge1xyXG5cdFx0cmV0dXJuIHtcclxuXHRcdFx0bmdNb2R1bGU6IEluZHVyb0xvZ01vZHVsZSxcclxuXHRcdFx0cHJvdmlkZXJzOiBbXHJcblx0XHRcdFx0eyBwcm92aWRlOiBMT0dHRVJfQ09ORklHLCB1c2VWYWx1ZTogY29uZmlnIH0sXHJcblx0XHRcdFx0eyBwcm92aWRlOiBERUZBVUxUX0xPR19aT05FLCB1c2VWYWx1ZTogY29uZmlnLnJvb3Rab25lIH0sXHJcblx0XHRcdF0sXHJcblx0XHR9O1xyXG5cdH1cclxufVxyXG4iXX0=