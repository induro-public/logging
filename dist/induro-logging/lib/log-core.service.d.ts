import { Observable } from 'rxjs';
import { LogAppender } from './appenders/log-appender.service';
import { LogMessage } from './models/log-message.model';
/**
 * Core log service that holds the pipeline for log messages.
 * Must be a singleton.
 * @export
 * @class LogCoreService
 */
export declare class LogCoreService {
    private messagesStream;
    constructor(appenders: LogAppender[]);
    /**
     * Observable stream of log messages.
     * Meant for appenders to subscribe on.
     * @readonly
     * @type {Observable<LogMessage>}
     * @memberof LogService
     */
    get messages(): Observable<LogMessage>;
    /**
     * Log a message.
     * @memberof LogCoreService
     */
    log(message: LogMessage): void;
}
