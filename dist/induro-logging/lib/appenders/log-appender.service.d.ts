import { LogMessage } from '../models/log-message.model';
export interface LogAppender {
    write(logMessage: LogMessage): void;
}
