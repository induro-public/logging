import { LogMessage } from '../models/log-message.model';
import { LogAppender } from './log-appender.service';
export declare class ConsoleAppender implements LogAppender {
    constructor();
    /**
     * Write a log message to the console.
     * @param logMessage
     * @memberof ConsoleAppender
     */
    write(logMessage: LogMessage): void;
}
