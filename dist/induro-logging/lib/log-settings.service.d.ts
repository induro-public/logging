import { Observable } from 'rxjs';
import { LoggingConfig } from './models/log-config.model';
import { LogLevel } from './models/log-level.model';
/**
 * Allow the user to modify the log settings live in the browser.
 * @export
 * @class LogSettingsService
 */
export declare class LogSettingsService {
    private logConfig;
    private configChanges;
    constructor(config: LoggingConfig);
    /**
     * Observe changes to the logging config.
     * @readonly
     * @type {Observable<LoggingConfig>}
     * @memberof LogSettingsService
     */
    get config(): Observable<LoggingConfig>;
    /**
     * Modify the log level.
     * @memberof LogSettingsService
     */
    set level(logLevel: LogLevel);
    /**
     * Set the zones.
     * @memberof LogSettingsService
     */
    set zones(zones: string[]);
    /**
     * Add a zone.
     * @memberof LogSettingsService
     */
    addZone(zone: string): void;
    /**
     * Remove a zone;
     * @memberof LogSettingsService
     */
    removeZone(zone: string): void;
    /**
     * Publish a change for the config.
     * @private
     * @memberof LogSettingsService
     */
    private updateConfig;
}
