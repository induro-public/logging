import { LogCoreService } from './log-core.service';
import { LogSettingsService } from './log-settings.service';
/**
 * Service used to persist log messages.
 * @export
 * @class LogService
 */
export declare class LogService {
    private logCore;
    for: (zone: string) => LogService;
    private zone;
    private config;
    constructor(logCore: LogCoreService, logSettings: LogSettingsService, zone?: string);
    /**
     * Is trace logging enabled?
     * @readonly
     * @memberof LogService
     */
    get isTraceEnabled(): boolean;
    /**
     * Is debug logging enabled?
     * @readonly
     * @memberof LogService
     */
    get isDebugEnabled(): boolean;
    /**
     * Is info logging enabled?
     * @readonly
     * @memberof LogService
     */
    get isInfoEnabled(): boolean;
    /**
     * Is warn logging enabled?
     * @readonly
     * @memberof LogService
     */
    get isWarnEnabled(): boolean;
    /**
     * Is error logging enabled?
     * @readonly
     * @memberof LogService
     */
    get isErrorEnabled(): boolean;
    /**
     * Is fatal logging enabled?
     * @readonly
     * @memberof LogService
     */
    get isFatalEnabled(): boolean;
    /**
     * Log a trace level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    trace(message: string, data?: any, exception?: Error): void;
    /**
     * Log a debug level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    debug(message: string, data?: any, exception?: Error): void;
    /**
     * Log an info level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    info(message: string, data?: any, exception?: Error): void;
    /**
     * Log a warn level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    warn(message: string, data?: any, exception?: Error): void;
    /**
     * Log an error level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    error(message: string, data?: any, exception?: Error): void;
    /**
     * Log a fatal level message if that level is enabled.
     * @param message Message to log
     * @param [data=null] Data that provides context for the message.
     * @param [exception] Error related to the message.
     * @memberof LogService
     */
    fatal(message: string, data?: any, exception?: Error): void;
    /**
     * Write a log message for the given level if that level is enabled.
     * @private
     * @param level
     * @param message
     * @param [data=null]
     * @param [exception]
     * @memberof LogService
     */
    private log;
    /**
     * Update the log settings.
     * @private
     * @param config
     * @memberof LogService
     */
    private onSettingsChange;
}
