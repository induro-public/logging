import { LogLevel } from './log-level.model';
export declare class LogMessage {
    level: LogLevel;
    timestamp: Date;
    message: string;
    zone?: string;
    data?: any;
    exception?: Error;
    constructor(level: LogLevel, timestamp: Date, message: string, zone?: string, data?: any, exception?: Error);
    /**
     * Get the human readable version of the log level.
     * @readonly
     * @memberof LogMessage
     */
    get levelText(): string;
}
