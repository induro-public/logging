/**
 * Designates the levels for log messages and configuration.
 * @export
 * @enum {number}
 */
export declare enum LogLevel {
    /**
     * Lowest level possible.
     */
    All = 0,
    /**
     * Fine-grained events used to track the flow of events.
     */
    Trace = 1,
    /**
     * Fine-grained informal events that are mostly useful to debug an application.
     */
    Debug = 2,
    /**
     * Informal message that highlights the progress of the application at coarse-grained level.
     */
    Info = 3,
    /**
     * Potentially harmful situations.
     */
    Warn = 4,
    /**
     * Error events that might still allow the application to continue running.
     */
    Error = 5,
    /**
     * Very severe error event that will presumably lead the application to abort.
     */
    Fatal = 6,
    /**
     * Highest level possible.
     */
    Off = 7
}
