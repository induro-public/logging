import { LogLevel } from './log-level.model';
/**
 * Intended to be used by the environment configuration
 */
export interface LoggingConfig {
    logLevel: LogLevel;
    rootZone: string;
    zones: Set<string>;
}
