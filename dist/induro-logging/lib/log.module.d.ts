import { InjectionToken, Provider } from '@angular/core';
import { LogAppender } from './appenders/log-appender.service';
import { LoggerSetupConfig } from './models/logger-setup-config.model';
export declare const LOGGER_CONFIG: InjectionToken<LoggerSetupConfig>;
export declare const DEFAULT_LOG_ZONE: InjectionToken<string>;
export declare const LOG_APPENDER: InjectionToken<LogAppender>;
export declare class InduroLogModule {
    static initializeLogger(config: LoggerSetupConfig): {
        ngModule: typeof InduroLogModule;
        providers: Provider[];
    };
}
