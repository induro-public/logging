import { __decorate, __param } from "tslib";
import { Subject } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { InduroLogModule, LOG_APPENDER } from './log.module';
import * as i0 from "@angular/core";
import * as i1 from "./log.module";
/**
 * Core log service that holds the pipeline for log messages.
 * Must be a singleton.
 * @export
 * @class LogCoreService
 */
let LogCoreService = class LogCoreService {
    constructor(appenders) {
        this.messagesStream = new Subject();
        this.messages.subscribe((logMessage) => appenders.forEach((app) => app.write(logMessage)));
    }
    /**
     * Observable stream of log messages.
     * Meant for appenders to subscribe on.
     * @readonly
     * @type {Observable<LogMessage>}
     * @memberof LogService
     */
    get messages() {
        return this.messagesStream.asObservable();
    }
    /**
     * Log a message.
     * @memberof LogCoreService
     */
    log(message) {
        this.messagesStream.next(message);
    }
};
LogCoreService.ctorParameters = () => [
    { type: Array, decorators: [{ type: Inject, args: [LOG_APPENDER,] }] }
];
LogCoreService.ɵprov = i0.ɵɵdefineInjectable({ factory: function LogCoreService_Factory() { return new LogCoreService(i0.ɵɵinject(i1.LOG_APPENDER)); }, token: LogCoreService, providedIn: i1.InduroLogModule });
LogCoreService = __decorate([
    Injectable({ providedIn: InduroLogModule }),
    __param(0, Inject(LOG_APPENDER))
], LogCoreService);
export { LogCoreService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLWNvcmUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BpbmR1cm8vbG9nZ2luZy8iLCJzb3VyY2VzIjpbImxpYi9sb2ctY29yZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBR25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsWUFBWSxFQUFFLE1BQU0sY0FBYyxDQUFDOzs7QUFHN0Q7Ozs7O0dBS0c7QUFFSCxJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBRzFCLFlBQWtDLFNBQXdCO1FBQ3pELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxPQUFPLEVBQWMsQ0FBQztRQUNoRCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILElBQVcsUUFBUTtRQUNsQixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVEOzs7T0FHRztJQUNJLEdBQUcsQ0FBQyxPQUFtQjtRQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuQyxDQUFDO0NBQ0QsQ0FBQTs7d0NBdkJhLE1BQU0sU0FBQyxZQUFZOzs7QUFIcEIsY0FBYztJQUQxQixVQUFVLENBQUMsRUFBRSxVQUFVLEVBQUUsZUFBZSxFQUFFLENBQUM7SUFJOUIsV0FBQSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUE7R0FIckIsY0FBYyxDQTBCMUI7U0ExQlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTG9nQXBwZW5kZXIgfSBmcm9tICcuL2FwcGVuZGVycy9sb2ctYXBwZW5kZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IEluZHVyb0xvZ01vZHVsZSwgTE9HX0FQUEVOREVSIH0gZnJvbSAnLi9sb2cubW9kdWxlJztcclxuaW1wb3J0IHsgTG9nTWVzc2FnZSB9IGZyb20gJy4vbW9kZWxzL2xvZy1tZXNzYWdlLm1vZGVsJztcclxuXHJcbi8qKlxyXG4gKiBDb3JlIGxvZyBzZXJ2aWNlIHRoYXQgaG9sZHMgdGhlIHBpcGVsaW5lIGZvciBsb2cgbWVzc2FnZXMuXHJcbiAqIE11c3QgYmUgYSBzaW5nbGV0b24uXHJcbiAqIEBleHBvcnRcclxuICogQGNsYXNzIExvZ0NvcmVTZXJ2aWNlXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46IEluZHVyb0xvZ01vZHVsZSB9KVxyXG5leHBvcnQgY2xhc3MgTG9nQ29yZVNlcnZpY2Uge1xyXG5cdHByaXZhdGUgbWVzc2FnZXNTdHJlYW06IFN1YmplY3Q8TG9nTWVzc2FnZT47XHJcblxyXG5cdGNvbnN0cnVjdG9yKEBJbmplY3QoTE9HX0FQUEVOREVSKSBhcHBlbmRlcnM6IExvZ0FwcGVuZGVyW10pIHtcclxuXHRcdHRoaXMubWVzc2FnZXNTdHJlYW0gPSBuZXcgU3ViamVjdDxMb2dNZXNzYWdlPigpO1xyXG5cdFx0dGhpcy5tZXNzYWdlcy5zdWJzY3JpYmUoKGxvZ01lc3NhZ2UpID0+IGFwcGVuZGVycy5mb3JFYWNoKChhcHApID0+IGFwcC53cml0ZShsb2dNZXNzYWdlKSkpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogT2JzZXJ2YWJsZSBzdHJlYW0gb2YgbG9nIG1lc3NhZ2VzLlxyXG5cdCAqIE1lYW50IGZvciBhcHBlbmRlcnMgdG8gc3Vic2NyaWJlIG9uLlxyXG5cdCAqIEByZWFkb25seVxyXG5cdCAqIEB0eXBlIHtPYnNlcnZhYmxlPExvZ01lc3NhZ2U+fVxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIGdldCBtZXNzYWdlcygpOiBPYnNlcnZhYmxlPExvZ01lc3NhZ2U+IHtcclxuXHRcdHJldHVybiB0aGlzLm1lc3NhZ2VzU3RyZWFtLmFzT2JzZXJ2YWJsZSgpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogTG9nIGEgbWVzc2FnZS5cclxuXHQgKiBAbWVtYmVyb2YgTG9nQ29yZVNlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgbG9nKG1lc3NhZ2U6IExvZ01lc3NhZ2UpOiB2b2lkIHtcclxuXHRcdHRoaXMubWVzc2FnZXNTdHJlYW0ubmV4dChtZXNzYWdlKTtcclxuXHR9XHJcbn1cclxuIl19