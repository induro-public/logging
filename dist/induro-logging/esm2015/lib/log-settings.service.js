import { __decorate, __param } from "tslib";
import { BehaviorSubject } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { InduroLogModule, LOGGER_CONFIG } from './log.module';
import * as i0 from "@angular/core";
import * as i1 from "./log.module";
/**
 * Allow the user to modify the log settings live in the browser.
 * @export
 * @class LogSettingsService
 */
let LogSettingsService = class LogSettingsService {
    constructor(config) {
        this.logConfig = config;
        this.configChanges = new BehaviorSubject(Object.assign({}, this.logConfig));
    }
    /**
     * Observe changes to the logging config.
     * @readonly
     * @type {Observable<LoggingConfig>}
     * @memberof LogSettingsService
     */
    get config() {
        return this.configChanges.asObservable();
    }
    /**
     * Modify the log level.
     * @memberof LogSettingsService
     */
    set level(logLevel) {
        this.logConfig.logLevel = logLevel;
        this.updateConfig();
    }
    /**
     * Set the zones.
     * @memberof LogSettingsService
     */
    set zones(zones) {
        this.logConfig.zones = new Set(zones);
        this.updateConfig();
    }
    /**
     * Add a zone.
     * @memberof LogSettingsService
     */
    addZone(zone) {
        if (this.logConfig.zones.has(zone)) {
            return;
        }
        this.logConfig.zones.add(zone);
        this.updateConfig();
    }
    /**
     * Remove a zone;
     * @memberof LogSettingsService
     */
    removeZone(zone) {
        if (!this.logConfig.zones.delete(zone)) {
            return;
        }
        this.updateConfig();
    }
    /**
     * Publish a change for the config.
     * @private
     * @memberof LogSettingsService
     */
    updateConfig() {
        this.configChanges.next(Object.assign({}, this.logConfig));
    }
};
LogSettingsService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [LOGGER_CONFIG,] }] }
];
LogSettingsService.ɵprov = i0.ɵɵdefineInjectable({ factory: function LogSettingsService_Factory() { return new LogSettingsService(i0.ɵɵinject(i1.LOGGER_CONFIG)); }, token: LogSettingsService, providedIn: i1.InduroLogModule });
LogSettingsService = __decorate([
    Injectable({ providedIn: InduroLogModule }),
    __param(0, Inject(LOGGER_CONFIG))
], LogSettingsService);
export { LogSettingsService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLXNldHRpbmdzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaW5kdXJvL2xvZ2dpbmcvIiwic291cmNlcyI6WyJsaWIvbG9nLXNldHRpbmdzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxlQUFlLEVBQXVCLE1BQU0sTUFBTSxDQUFDO0FBRTVELE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5ELE9BQU8sRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDOzs7QUFJOUQ7Ozs7R0FJRztBQUVILElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBSTlCLFlBQW1DLE1BQXFCO1FBQ3ZELElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxlQUFlLG1CQUFxQixJQUFJLENBQUMsU0FBUyxFQUFHLENBQUM7SUFDaEYsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsSUFBVyxNQUFNO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxLQUFLLENBQUMsUUFBa0I7UUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ25DLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxLQUFLLENBQUMsS0FBZTtRQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxJQUFJLEdBQUcsQ0FBUyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLE9BQU8sQ0FBQyxJQUFZO1FBQzFCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLE9BQU87U0FDUDtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRztJQUNJLFVBQVUsQ0FBQyxJQUFZO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDdkMsT0FBTztTQUNQO1FBQ0QsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ssWUFBWTtRQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksbUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRyxDQUFDO0lBQ2hELENBQUM7Q0FDRCxDQUFBOzs0Q0FoRWEsTUFBTSxTQUFDLGFBQWE7OztBQUpyQixrQkFBa0I7SUFEOUIsVUFBVSxDQUFDLEVBQUUsVUFBVSxFQUFFLGVBQWUsRUFBRSxDQUFDO0lBSzlCLFdBQUEsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0dBSnRCLGtCQUFrQixDQW9FOUI7U0FwRVksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IEluZHVyb0xvZ01vZHVsZSwgTE9HR0VSX0NPTkZJRyB9IGZyb20gJy4vbG9nLm1vZHVsZSc7XHJcbmltcG9ydCB7IExvZ2dpbmdDb25maWcgfSBmcm9tICcuL21vZGVscy9sb2ctY29uZmlnLm1vZGVsJztcclxuaW1wb3J0IHsgTG9nTGV2ZWwgfSBmcm9tICcuL21vZGVscy9sb2ctbGV2ZWwubW9kZWwnO1xyXG5cclxuLyoqXHJcbiAqIEFsbG93IHRoZSB1c2VyIHRvIG1vZGlmeSB0aGUgbG9nIHNldHRpbmdzIGxpdmUgaW4gdGhlIGJyb3dzZXIuXHJcbiAqIEBleHBvcnRcclxuICogQGNsYXNzIExvZ1NldHRpbmdzU2VydmljZVxyXG4gKi9cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiBJbmR1cm9Mb2dNb2R1bGUgfSlcclxuZXhwb3J0IGNsYXNzIExvZ1NldHRpbmdzU2VydmljZSB7XHJcblx0cHJpdmF0ZSBsb2dDb25maWc6IExvZ2dpbmdDb25maWc7XHJcblx0cHJpdmF0ZSBjb25maWdDaGFuZ2VzOiBTdWJqZWN0PExvZ2dpbmdDb25maWc+O1xyXG5cclxuXHRjb25zdHJ1Y3RvcihASW5qZWN0KExPR0dFUl9DT05GSUcpIGNvbmZpZzogTG9nZ2luZ0NvbmZpZykge1xyXG5cdFx0dGhpcy5sb2dDb25maWcgPSBjb25maWc7XHJcblx0XHR0aGlzLmNvbmZpZ0NoYW5nZXMgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PExvZ2dpbmdDb25maWc+KHsgLi4udGhpcy5sb2dDb25maWcgfSk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBPYnNlcnZlIGNoYW5nZXMgdG8gdGhlIGxvZ2dpbmcgY29uZmlnLlxyXG5cdCAqIEByZWFkb25seVxyXG5cdCAqIEB0eXBlIHtPYnNlcnZhYmxlPExvZ2dpbmdDb25maWc+fVxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXR0aW5nc1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgZ2V0IGNvbmZpZygpOiBPYnNlcnZhYmxlPExvZ2dpbmdDb25maWc+IHtcclxuXHRcdHJldHVybiB0aGlzLmNvbmZpZ0NoYW5nZXMuYXNPYnNlcnZhYmxlKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBNb2RpZnkgdGhlIGxvZyBsZXZlbC5cclxuXHQgKiBAbWVtYmVyb2YgTG9nU2V0dGluZ3NTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIHNldCBsZXZlbChsb2dMZXZlbDogTG9nTGV2ZWwpIHtcclxuXHRcdHRoaXMubG9nQ29uZmlnLmxvZ0xldmVsID0gbG9nTGV2ZWw7XHJcblx0XHR0aGlzLnVwZGF0ZUNvbmZpZygpO1xyXG5cdH1cclxuXHJcblx0LyoqXHJcblx0ICogU2V0IHRoZSB6b25lcy5cclxuXHQgKiBAbWVtYmVyb2YgTG9nU2V0dGluZ3NTZXJ2aWNlXHJcblx0ICovXHJcblx0cHVibGljIHNldCB6b25lcyh6b25lczogc3RyaW5nW10pIHtcclxuXHRcdHRoaXMubG9nQ29uZmlnLnpvbmVzID0gbmV3IFNldDxzdHJpbmc+KHpvbmVzKTtcclxuXHRcdHRoaXMudXBkYXRlQ29uZmlnKCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBBZGQgYSB6b25lLlxyXG5cdCAqIEBtZW1iZXJvZiBMb2dTZXR0aW5nc1NlcnZpY2VcclxuXHQgKi9cclxuXHRwdWJsaWMgYWRkWm9uZSh6b25lOiBzdHJpbmcpOiB2b2lkIHtcclxuXHRcdGlmICh0aGlzLmxvZ0NvbmZpZy56b25lcy5oYXMoem9uZSkpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5sb2dDb25maWcuem9uZXMuYWRkKHpvbmUpO1xyXG5cdFx0dGhpcy51cGRhdGVDb25maWcoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFJlbW92ZSBhIHpvbmU7XHJcblx0ICogQG1lbWJlcm9mIExvZ1NldHRpbmdzU2VydmljZVxyXG5cdCAqL1xyXG5cdHB1YmxpYyByZW1vdmVab25lKHpvbmU6IHN0cmluZyk6IHZvaWQge1xyXG5cdFx0aWYgKCF0aGlzLmxvZ0NvbmZpZy56b25lcy5kZWxldGUoem9uZSkpIHtcclxuXHRcdFx0cmV0dXJuO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy51cGRhdGVDb25maWcoKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIFB1Ymxpc2ggYSBjaGFuZ2UgZm9yIHRoZSBjb25maWcuXHJcblx0ICogQHByaXZhdGVcclxuXHQgKiBAbWVtYmVyb2YgTG9nU2V0dGluZ3NTZXJ2aWNlXHJcblx0ICovXHJcblx0cHJpdmF0ZSB1cGRhdGVDb25maWcoKTogdm9pZCB7XHJcblx0XHR0aGlzLmNvbmZpZ0NoYW5nZXMubmV4dCh7IC4uLnRoaXMubG9nQ29uZmlnIH0pO1xyXG5cdH1cclxufVxyXG4iXX0=