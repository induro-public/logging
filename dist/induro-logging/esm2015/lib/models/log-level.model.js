/**
 * Designates the levels for log messages and configuration.
 * @export
 * @enum {number}
 */
export var LogLevel;
(function (LogLevel) {
    /**
     * Lowest level possible.
     */
    LogLevel[LogLevel["All"] = 0] = "All";
    /**
     * Fine-grained events used to track the flow of events.
     */
    LogLevel[LogLevel["Trace"] = 1] = "Trace";
    /**
     * Fine-grained informal events that are mostly useful to debug an application.
     */
    LogLevel[LogLevel["Debug"] = 2] = "Debug";
    /**
     * Informal message that highlights the progress of the application at coarse-grained level.
     */
    LogLevel[LogLevel["Info"] = 3] = "Info";
    /**
     * Potentially harmful situations.
     */
    LogLevel[LogLevel["Warn"] = 4] = "Warn";
    /**
     * Error events that might still allow the application to continue running.
     */
    LogLevel[LogLevel["Error"] = 5] = "Error";
    /**
     * Very severe error event that will presumably lead the application to abort.
     */
    LogLevel[LogLevel["Fatal"] = 6] = "Fatal";
    /**
     * Highest level possible.
     */
    LogLevel[LogLevel["Off"] = 7] = "Off";
})(LogLevel || (LogLevel = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLWxldmVsLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGluZHVyby9sb2dnaW5nLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9sb2ctbGV2ZWwubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7R0FJRztBQUNILE1BQU0sQ0FBTixJQUFZLFFBaUNYO0FBakNELFdBQVksUUFBUTtJQUNuQjs7T0FFRztJQUNILHFDQUFHLENBQUE7SUFDSDs7T0FFRztJQUNILHlDQUFLLENBQUE7SUFDTDs7T0FFRztJQUNILHlDQUFLLENBQUE7SUFDTDs7T0FFRztJQUNILHVDQUFJLENBQUE7SUFDSjs7T0FFRztJQUNILHVDQUFJLENBQUE7SUFDSjs7T0FFRztJQUNILHlDQUFLLENBQUE7SUFDTDs7T0FFRztJQUNILHlDQUFLLENBQUE7SUFDTDs7T0FFRztJQUNILHFDQUFHLENBQUE7QUFDSixDQUFDLEVBakNXLFFBQVEsS0FBUixRQUFRLFFBaUNuQiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBEZXNpZ25hdGVzIHRoZSBsZXZlbHMgZm9yIGxvZyBtZXNzYWdlcyBhbmQgY29uZmlndXJhdGlvbi5cclxuICogQGV4cG9ydFxyXG4gKiBAZW51bSB7bnVtYmVyfVxyXG4gKi9cclxuZXhwb3J0IGVudW0gTG9nTGV2ZWwge1xyXG5cdC8qKlxyXG5cdCAqIExvd2VzdCBsZXZlbCBwb3NzaWJsZS5cclxuXHQgKi9cclxuXHRBbGwsXHJcblx0LyoqXHJcblx0ICogRmluZS1ncmFpbmVkIGV2ZW50cyB1c2VkIHRvIHRyYWNrIHRoZSBmbG93IG9mIGV2ZW50cy5cclxuXHQgKi9cclxuXHRUcmFjZSxcclxuXHQvKipcclxuXHQgKiBGaW5lLWdyYWluZWQgaW5mb3JtYWwgZXZlbnRzIHRoYXQgYXJlIG1vc3RseSB1c2VmdWwgdG8gZGVidWcgYW4gYXBwbGljYXRpb24uXHJcblx0ICovXHJcblx0RGVidWcsXHJcblx0LyoqXHJcblx0ICogSW5mb3JtYWwgbWVzc2FnZSB0aGF0IGhpZ2hsaWdodHMgdGhlIHByb2dyZXNzIG9mIHRoZSBhcHBsaWNhdGlvbiBhdCBjb2Fyc2UtZ3JhaW5lZCBsZXZlbC5cclxuXHQgKi9cclxuXHRJbmZvLFxyXG5cdC8qKlxyXG5cdCAqIFBvdGVudGlhbGx5IGhhcm1mdWwgc2l0dWF0aW9ucy5cclxuXHQgKi9cclxuXHRXYXJuLFxyXG5cdC8qKlxyXG5cdCAqIEVycm9yIGV2ZW50cyB0aGF0IG1pZ2h0IHN0aWxsIGFsbG93IHRoZSBhcHBsaWNhdGlvbiB0byBjb250aW51ZSBydW5uaW5nLlxyXG5cdCAqL1xyXG5cdEVycm9yLFxyXG5cdC8qKlxyXG5cdCAqIFZlcnkgc2V2ZXJlIGVycm9yIGV2ZW50IHRoYXQgd2lsbCBwcmVzdW1hYmx5IGxlYWQgdGhlIGFwcGxpY2F0aW9uIHRvIGFib3J0LlxyXG5cdCAqL1xyXG5cdEZhdGFsLFxyXG5cdC8qKlxyXG5cdCAqIEhpZ2hlc3QgbGV2ZWwgcG9zc2libGUuXHJcblx0ICovXHJcblx0T2ZmXHJcbn1cclxuIl19