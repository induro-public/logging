(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define('@induro/logging', ['exports', '@angular/core', 'rxjs'], factory) :
    (global = global || self, factory((global.induro = global.induro || {}, global.induro.logging = {}), global.ng.core, global.rxjs));
}(this, (function (exports, core, rxjs) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var LOGGER_CONFIG = new core.InjectionToken('induro.logging.config');
    var DEFAULT_LOG_ZONE = new core.InjectionToken('induro.logging.defaultZone');
    var LOG_APPENDER = new core.InjectionToken('induro.logging.appender');
    var InduroLogModule = /** @class */ (function () {
        function InduroLogModule() {
        }
        InduroLogModule_1 = InduroLogModule;
        InduroLogModule.initializeLogger = function (config) {
            return {
                ngModule: InduroLogModule_1,
                providers: [
                    { provide: LOGGER_CONFIG, useValue: config },
                    { provide: DEFAULT_LOG_ZONE, useValue: config.rootZone },
                ],
            };
        };
        var InduroLogModule_1;
        InduroLogModule = InduroLogModule_1 = __decorate([
            core.NgModule()
        ], InduroLogModule);
        return InduroLogModule;
    }());

    /**
     * Core log service that holds the pipeline for log messages.
     * Must be a singleton.
     * @export
     * @class LogCoreService
     */
    var LogCoreService = /** @class */ (function () {
        function LogCoreService(appenders) {
            this.messagesStream = new rxjs.Subject();
            this.messages.subscribe(function (logMessage) { return appenders.forEach(function (app) { return app.write(logMessage); }); });
        }
        Object.defineProperty(LogCoreService.prototype, "messages", {
            /**
             * Observable stream of log messages.
             * Meant for appenders to subscribe on.
             * @readonly
             * @type {Observable<LogMessage>}
             * @memberof LogService
             */
            get: function () {
                return this.messagesStream.asObservable();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Log a message.
         * @memberof LogCoreService
         */
        LogCoreService.prototype.log = function (message) {
            this.messagesStream.next(message);
        };
        LogCoreService.ctorParameters = function () { return [
            { type: Array, decorators: [{ type: core.Inject, args: [LOG_APPENDER,] }] }
        ]; };
        LogCoreService.ɵprov = core.ɵɵdefineInjectable({ factory: function LogCoreService_Factory() { return new LogCoreService(core.ɵɵinject(LOG_APPENDER)); }, token: LogCoreService, providedIn: InduroLogModule });
        LogCoreService = __decorate([
            core.Injectable({ providedIn: InduroLogModule }),
            __param(0, core.Inject(LOG_APPENDER))
        ], LogCoreService);
        return LogCoreService;
    }());

    /**
     * Allow the user to modify the log settings live in the browser.
     * @export
     * @class LogSettingsService
     */
    var LogSettingsService = /** @class */ (function () {
        function LogSettingsService(config) {
            this.logConfig = config;
            this.configChanges = new rxjs.BehaviorSubject(__assign({}, this.logConfig));
        }
        Object.defineProperty(LogSettingsService.prototype, "config", {
            /**
             * Observe changes to the logging config.
             * @readonly
             * @type {Observable<LoggingConfig>}
             * @memberof LogSettingsService
             */
            get: function () {
                return this.configChanges.asObservable();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogSettingsService.prototype, "level", {
            /**
             * Modify the log level.
             * @memberof LogSettingsService
             */
            set: function (logLevel) {
                this.logConfig.logLevel = logLevel;
                this.updateConfig();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogSettingsService.prototype, "zones", {
            /**
             * Set the zones.
             * @memberof LogSettingsService
             */
            set: function (zones) {
                this.logConfig.zones = new Set(zones);
                this.updateConfig();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Add a zone.
         * @memberof LogSettingsService
         */
        LogSettingsService.prototype.addZone = function (zone) {
            if (this.logConfig.zones.has(zone)) {
                return;
            }
            this.logConfig.zones.add(zone);
            this.updateConfig();
        };
        /**
         * Remove a zone;
         * @memberof LogSettingsService
         */
        LogSettingsService.prototype.removeZone = function (zone) {
            if (!this.logConfig.zones.delete(zone)) {
                return;
            }
            this.updateConfig();
        };
        /**
         * Publish a change for the config.
         * @private
         * @memberof LogSettingsService
         */
        LogSettingsService.prototype.updateConfig = function () {
            this.configChanges.next(__assign({}, this.logConfig));
        };
        LogSettingsService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [LOGGER_CONFIG,] }] }
        ]; };
        LogSettingsService.ɵprov = core.ɵɵdefineInjectable({ factory: function LogSettingsService_Factory() { return new LogSettingsService(core.ɵɵinject(LOGGER_CONFIG)); }, token: LogSettingsService, providedIn: InduroLogModule });
        LogSettingsService = __decorate([
            core.Injectable({ providedIn: InduroLogModule }),
            __param(0, core.Inject(LOGGER_CONFIG))
        ], LogSettingsService);
        return LogSettingsService;
    }());

    /**
     * Designates the levels for log messages and configuration.
     * @export
     * @enum {number}
     */

    (function (LogLevel) {
        /**
         * Lowest level possible.
         */
        LogLevel[LogLevel["All"] = 0] = "All";
        /**
         * Fine-grained events used to track the flow of events.
         */
        LogLevel[LogLevel["Trace"] = 1] = "Trace";
        /**
         * Fine-grained informal events that are mostly useful to debug an application.
         */
        LogLevel[LogLevel["Debug"] = 2] = "Debug";
        /**
         * Informal message that highlights the progress of the application at coarse-grained level.
         */
        LogLevel[LogLevel["Info"] = 3] = "Info";
        /**
         * Potentially harmful situations.
         */
        LogLevel[LogLevel["Warn"] = 4] = "Warn";
        /**
         * Error events that might still allow the application to continue running.
         */
        LogLevel[LogLevel["Error"] = 5] = "Error";
        /**
         * Very severe error event that will presumably lead the application to abort.
         */
        LogLevel[LogLevel["Fatal"] = 6] = "Fatal";
        /**
         * Highest level possible.
         */
        LogLevel[LogLevel["Off"] = 7] = "Off";
    })(exports.LogLevel || (exports.LogLevel = {}));

    var LogMessage = /** @class */ (function () {
        function LogMessage(level, timestamp, message, zone, data, exception) {
            this.level = level;
            this.timestamp = timestamp;
            this.message = message;
            this.zone = zone;
            this.data = data;
            this.exception = exception;
        }
        Object.defineProperty(LogMessage.prototype, "levelText", {
            /**
             * Get the human readable version of the log level.
             * @readonly
             * @memberof LogMessage
             */
            // eslint-disable-next-line getter-return, consistent-return
            get: function () {
                // eslint-disable-next-line default-case
                switch (this.level) {
                    case exports.LogLevel.All:
                        return 'ALL';
                    case exports.LogLevel.Trace:
                        return 'TRACE';
                    case exports.LogLevel.Debug:
                        return 'DEBUG';
                    case exports.LogLevel.Info:
                        return 'INFO';
                    case exports.LogLevel.Warn:
                        return 'WARN';
                    case exports.LogLevel.Error:
                        return 'ERROR';
                    case exports.LogLevel.Fatal:
                        return 'FATAL';
                    case exports.LogLevel.Off:
                    default:
                        return 'OFF';
                }
            },
            enumerable: true,
            configurable: true
        });
        return LogMessage;
    }());

    /**
     * Service used to persist log messages.
     * @export
     * @class LogService
     */
    var LogService = /** @class */ (function () {
        // need a provider for log zone for the --prod build
        function LogService(logCore, logSettings, zone) {
            var _this = this;
            this.logCore = logCore;
            this.zone = zone;
            logSettings.config.subscribe(function (config) { return _this.onSettingsChange(config); });
            // setup method to create a zoned logger
            this.for = function (z) { return new LogService_1(logCore, logSettings, z); };
        }
        LogService_1 = LogService;
        Object.defineProperty(LogService.prototype, "isTraceEnabled", {
            /**
             * Is trace logging enabled?
             * @readonly
             * @memberof LogService
             */
            get: function () {
                return this.config.logLevel <= exports.LogLevel.Trace;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogService.prototype, "isDebugEnabled", {
            /**
             * Is debug logging enabled?
             * @readonly
             * @memberof LogService
             */
            get: function () {
                return this.config.logLevel <= exports.LogLevel.Debug;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogService.prototype, "isInfoEnabled", {
            /**
             * Is info logging enabled?
             * @readonly
             * @memberof LogService
             */
            get: function () {
                return this.config.logLevel <= exports.LogLevel.Info;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogService.prototype, "isWarnEnabled", {
            /**
             * Is warn logging enabled?
             * @readonly
             * @memberof LogService
             */
            get: function () {
                return this.config.logLevel <= exports.LogLevel.Warn;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogService.prototype, "isErrorEnabled", {
            /**
             * Is error logging enabled?
             * @readonly
             * @memberof LogService
             */
            get: function () {
                return this.config.logLevel <= exports.LogLevel.Error;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(LogService.prototype, "isFatalEnabled", {
            /**
             * Is fatal logging enabled?
             * @readonly
             * @memberof LogService
             */
            get: function () {
                return this.config.logLevel <= exports.LogLevel.Fatal;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * Log a trace level message if that level is enabled.
         * @param message Message to log
         * @param [data=null] Data that provides context for the message.
         * @param [exception] Error related to the message.
         * @memberof LogService
         */
        LogService.prototype.trace = function (message, data, exception) {
            if (data === void 0) { data = null; }
            this.log(exports.LogLevel.Trace, message, data, exception);
        };
        /**
         * Log a debug level message if that level is enabled.
         * @param message Message to log
         * @param [data=null] Data that provides context for the message.
         * @param [exception] Error related to the message.
         * @memberof LogService
         */
        LogService.prototype.debug = function (message, data, exception) {
            if (data === void 0) { data = null; }
            this.log(exports.LogLevel.Debug, message, data, exception);
        };
        /**
         * Log an info level message if that level is enabled.
         * @param message Message to log
         * @param [data=null] Data that provides context for the message.
         * @param [exception] Error related to the message.
         * @memberof LogService
         */
        LogService.prototype.info = function (message, data, exception) {
            if (data === void 0) { data = null; }
            this.log(exports.LogLevel.Info, message, data, exception);
        };
        /**
         * Log a warn level message if that level is enabled.
         * @param message Message to log
         * @param [data=null] Data that provides context for the message.
         * @param [exception] Error related to the message.
         * @memberof LogService
         */
        LogService.prototype.warn = function (message, data, exception) {
            if (data === void 0) { data = null; }
            this.log(exports.LogLevel.Warn, message, data, exception);
        };
        /**
         * Log an error level message if that level is enabled.
         * @param message Message to log
         * @param [data=null] Data that provides context for the message.
         * @param [exception] Error related to the message.
         * @memberof LogService
         */
        LogService.prototype.error = function (message, data, exception) {
            if (data === void 0) { data = null; }
            this.log(exports.LogLevel.Error, message, data, exception);
        };
        /**
         * Log a fatal level message if that level is enabled.
         * @param message Message to log
         * @param [data=null] Data that provides context for the message.
         * @param [exception] Error related to the message.
         * @memberof LogService
         */
        LogService.prototype.fatal = function (message, data, exception) {
            if (data === void 0) { data = null; }
            this.log(exports.LogLevel.Fatal, message, data, exception);
        };
        /**
         * Write a log message for the given level if that level is enabled.
         * @private
         * @param level
         * @param message
         * @param [data=null]
         * @param [exception]
         * @memberof LogService
         */
        LogService.prototype.log = function (level, message, data, exception) {
            if (data === void 0) { data = null; }
            // filter by log level
            if (this.config.logLevel > level) {
                return;
            }
            // filter by zone
            if (this.config.zones.size > 0 && (!this.zone || !this.config.zones.has(this.zone))) {
                return;
            }
            var logMessage = new LogMessage(level, new Date(), message, this.zone, data, exception);
            this.logCore.log(logMessage);
        };
        /**
         * Update the log settings.
         * @private
         * @param config
         * @memberof LogService
         */
        LogService.prototype.onSettingsChange = function (config) {
            this.config = config;
        };
        var LogService_1;
        LogService.ctorParameters = function () { return [
            { type: LogCoreService },
            { type: LogSettingsService },
            { type: String, decorators: [{ type: core.Inject, args: [DEFAULT_LOG_ZONE,] }] }
        ]; };
        LogService.ɵprov = core.ɵɵdefineInjectable({ factory: function LogService_Factory() { return new LogService(core.ɵɵinject(LogCoreService), core.ɵɵinject(LogSettingsService), core.ɵɵinject(DEFAULT_LOG_ZONE)); }, token: LogService, providedIn: "root" });
        LogService = LogService_1 = __decorate([
            core.Injectable({ providedIn: 'root' }),
            __param(2, core.Inject(DEFAULT_LOG_ZONE))
        ], LogService);
        return LogService;
    }());

    var LOG_LEVEL_STYLES = new Map();
    LOG_LEVEL_STYLES.set(exports.LogLevel.Trace, 'font-weight: bold; color: #B0BEC5;');
    LOG_LEVEL_STYLES.set(exports.LogLevel.Debug, 'font-weight: bold; color: #4CAF50;');
    LOG_LEVEL_STYLES.set(exports.LogLevel.Info, 'font-weight: bold; color: #0277BD;');
    LOG_LEVEL_STYLES.set(exports.LogLevel.Warn, 'font-weight: bold; color: #FFC107;');
    LOG_LEVEL_STYLES.set(exports.LogLevel.Error, 'font-weight: bold; color: #B71C1C;');
    LOG_LEVEL_STYLES.set(exports.LogLevel.Fatal, 'font-weight: bold; color: #B71C1C;');
    var ConsoleAppender = /** @class */ (function () {
        function ConsoleAppender() {
        }
        /**
         * Write a log message to the console.
         * @param logMessage
         * @memberof ConsoleAppender
         */
        ConsoleAppender.prototype.write = function (logMessage) {
            var style = LOG_LEVEL_STYLES.get(logMessage.level);
            if (logMessage.exception) {
                console.groupCollapsed(logMessage.timestamp.toLocaleString() + " [" + logMessage.levelText + "] (" + logMessage.zone + ")");
                console.log("%c" + logMessage.message, style);
                if (logMessage.data != null) {
                    console.log(logMessage.data);
                }
                if (logMessage.exception) {
                    console.log(logMessage.exception);
                }
                console.groupEnd();
            }
            else {
                var message = "%c" + logMessage.timestamp.toLocaleString() + " [" + logMessage.levelText + "] (" + logMessage.zone + ") - %c" + logMessage.message;
                if (logMessage.data != null) {
                    console.log(message, style, '', logMessage.data);
                }
                else {
                    console.log(message, style, '');
                }
            }
        };
        ConsoleAppender.ɵprov = core.ɵɵdefineInjectable({ factory: function ConsoleAppender_Factory() { return new ConsoleAppender(); }, token: ConsoleAppender, providedIn: InduroLogModule });
        ConsoleAppender = __decorate([
            core.Injectable({ providedIn: InduroLogModule })
        ], ConsoleAppender);
        return ConsoleAppender;
    }());

    exports.ConsoleAppender = ConsoleAppender;
    exports.InduroLogModule = InduroLogModule;
    exports.LOG_APPENDER = LOG_APPENDER;
    exports.LogService = LogService;
    exports.ɵa = LOGGER_CONFIG;
    exports.ɵb = DEFAULT_LOG_ZONE;
    exports.ɵc = LogCoreService;
    exports.ɵd = LogSettingsService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=induro-logging.umd.js.map
