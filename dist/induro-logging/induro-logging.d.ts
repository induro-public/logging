/**
 * Generated bundle index. Do not edit.
 */
export * from './public-api';
export { LogCoreService as ɵc } from './lib/log-core.service';
export { LogSettingsService as ɵd } from './lib/log-settings.service';
export { DEFAULT_LOG_ZONE as ɵb, LOGGER_CONFIG as ɵa } from './lib/log.module';
export { LoggingConfig as ɵe } from './lib/models/log-config.model';
