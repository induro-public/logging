import { InjectionToken } from '@angular/core';
import { inject, TestBed } from '@angular/core/testing';

import { ConsoleAppender } from './appenders/console-appender.service';
import { LogCoreService } from './log-core.service';
import { LogSettingsService } from './log-settings.service';
import { LogService } from './log.service';
import { LoggingConfig } from './models/log-config.model';
import { LogLevel } from './models/log-level.model';
import { LogMessage } from './models/log-message.model';

describe('LogService', () => {
	describe('Log Levels', () => {
		beforeEach(() => {
			TestBed.configureTestingModule({
				providers: [
					LogService,
					LogSettingsService,
					LogCoreService,
					ConsoleAppender,
					{
						provide: InjectionToken,
						useValue: {
							logging: {
								logLevel: LogLevel.All,
								zones: new Set<string>([]),
							} as LoggingConfig,
						},
					},
				],
			});
		});

		const testLevel = (
			service: LogService,
			coreService: LogCoreService,
			level: LogLevel,
			method: (message: string, data?: any, exception?: Error) => void
		): void => {
			// arrange
			const message = 'test';
			const data = {};
			const exception = new Error('boom');
			const now = new Date();
			let actualLogMessage: LogMessage;
			coreService.messages.subscribe((x) => {
				actualLogMessage = x;
			});

			// act
			method.call(service, message, data, exception);

			// assert
			expect(actualLogMessage!).toBeTruthy();
			expect(actualLogMessage!.level).toBe(level);
			expect(actualLogMessage!.message).toBe(message);
			expect(actualLogMessage!.data).toBe(data);
			expect(actualLogMessage!.exception).toBe(exception);
			expect(actualLogMessage!.timestamp >= now).toBeTruthy();
		};

		it('should be created', inject([LogService, LogCoreService], (service: LogService) => {
			expect(service).toBeTruthy();
		}));

		it('should log a trace message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				testLevel(service, coreService, LogLevel.Trace, service.trace);
			}
		));

		it('should log a debug message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				testLevel(service, coreService, LogLevel.Debug, service.debug);
			}
		));

		it('should log a info message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				testLevel(service, coreService, LogLevel.Info, service.info);
			}
		));

		it('should log a warn message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				testLevel(service, coreService, LogLevel.Warn, service.warn);
			}
		));

		it('should log a Error message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				testLevel(service, coreService, LogLevel.Error, service.error);
			}
		));

		it('should log a Fatal message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				testLevel(service, coreService, LogLevel.Fatal, service.fatal);
			}
		));
	});

	describe('NotEnabled', () => {
		beforeEach(() => {
			TestBed.configureTestingModule({
				providers: [
					LogService,
					LogSettingsService,
					LogCoreService,
					ConsoleAppender,
					{
						provide: InjectionToken,
						useValue: {
							logging: {
								logLevel: LogLevel.Off,
								zones: new Set<string>([]),
							} as LoggingConfig,
						},
					},
				],
			});
		});

		const testIsEnabled = (
			service: LogService,
			coreService: LogCoreService,
			level: LogLevel,
			method: (message: string, data?: any, exception?: Error) => void
		): void => {
			// arrange
			const message = 'test';
			const data = {};
			const exception = new Error('boom');
			let actualLogMessage: LogMessage | null = null;
			coreService.messages.subscribe((x) => {
				actualLogMessage = x;
			});

			// act
			method.call(service, message, data, exception);

			// assert
			expect(actualLogMessage).toBeNull();
		};

		it('should not log a trace message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				expect(service.isTraceEnabled).toBeFalsy();
				testIsEnabled(service, coreService, LogLevel.Trace, service.trace);
			}
		));

		it('should not log a debug message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				expect(service.isDebugEnabled).toBeFalsy();
				testIsEnabled(service, coreService, LogLevel.Debug, service.debug);
			}
		));

		it('should not log a info message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				expect(service.isInfoEnabled).toBeFalsy();
				testIsEnabled(service, coreService, LogLevel.Info, service.info);
			}
		));

		it('should not log a warn message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				expect(service.isWarnEnabled).toBeFalsy();
				testIsEnabled(service, coreService, LogLevel.Warn, service.warn);
			}
		));

		it('should not log a Error message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				expect(service.isErrorEnabled).toBeFalsy();
				testIsEnabled(service, coreService, LogLevel.Error, service.error);
			}
		));

		it('should not log a Fatal message', inject(
			[LogService, LogCoreService],
			(service: LogService, coreService: LogCoreService) => {
				expect(service.isFatalEnabled).toBeFalsy();
				testIsEnabled(service, coreService, LogLevel.Fatal, service.fatal);
			}
		));
	});
});
