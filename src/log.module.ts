import { InjectionToken, NgModule, Provider } from '@angular/core';

import { LogAppender } from './appenders/log-appender.service';
import { LoggerSetupConfig } from './models/logger-setup-config.model';

export const LOGGER_CONFIG = new InjectionToken<LoggerSetupConfig>('induro.logging.config');
export const DEFAULT_LOG_ZONE: InjectionToken<string> = new InjectionToken<string>('induro.logging.defaultZone');
export const LOG_APPENDER: InjectionToken<LogAppender> = new InjectionToken<string>('induro.logging.appender');

@NgModule()
export class InduroLogModule {
	static initializeLogger(config: LoggerSetupConfig): { ngModule: typeof InduroLogModule; providers: Provider[] } {
		return {
			ngModule: InduroLogModule,
			providers: [
				{ provide: LOGGER_CONFIG, useValue: config },
				{ provide: DEFAULT_LOG_ZONE, useValue: config.rootZone },
			],
		};
	}
}
